#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file T_F_Test.py
@brief       Code for button push test to record user reaction time when seeing lit LED. 
@details     Once run, the code waits 2-3 seconds and lights LED. It gives the user 1 second 
             to press button PC13 and records that counter time. It utilizes an interrupt mechanism when the button is pushed. It gives an error message if 
             user doesn't push the button within the interval and averages all reaction times after cntrl C is pressed.
            
             Link to Source Code: https://bitbucket.org/ahumpal/me_405labs/src/master/lab02/T_F_Test.py
                 
@author      Ashley Humpal
@copyright   License Info
@date        January 23, 2021
"""


# Import any required modules
import pyb
import micropython
import urandom

# Create emergency buffer to store errors that are thrown inside ISR
micropython.alloc_emergency_exception_buf(200) #for troubleshooting



## List representing reaction time 
times=[] 

## Timer variable initialization 
t=0

## Pin object to use for blinking LED. Attached to PA5
myPin = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

## Pin object to use for PC13
bluePin = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)

## Create a timer object (timer 2) in general purpose counting mode
myTimer = pyb.Timer(2, prescaler=79, period=0x7FFFFFFF)

def myCallback(timSource):
   """Function called by interrupt to record timer count value and turn off LED. 
   @brief          Turns LED off and sets timer counter value.
   @details        Makes counter value a global variable
   """    
   myPin.low() #turn off LED

## Timer counter value set to global variable   
   global t #create global variable to send
   t=myTimer.counter() #timer counter variable created
   
   
   
## Variable configuring interrupt

extint = pyb.ExtInt (bluePin,                    # Which pin
             pyb.ExtInt.IRQ_FALLING,       # Interrupt on falling edge
             pyb.Pin.PULL_UP,              # Activate pullup resistor
             myCallback)                   # Interrupt service routine


while True: #While loop created
    try: #do the following 
## Variable for random value between 2 and 3 seconds
        x= urandom.randrange(2000, 3000) #selects random value between 2 and 3 seconds
        pyb.delay(x)  # delays code for whatever value of x is
        t=0 #zero timer counter global variable
        myTimer.counter(0)#zero timer counter value
        myPin.high() #turn LED on
        pyb.delay(1000) #delay code for 1 second
        myPin.low() #turn off LED
        if t< 1000000 and t>0: # if global variable between 0 and 1 sec do following
            times.append(t) #append t to list 
            #print(times)
        else:
            print('Too slow. Try again.') #error handling if user doesn't press button 
    except KeyboardInterrupt: #if cntrl C then do following
## Variable for average reaction time in seconds
          Avg= (sum(times)/len(times))/1000000 #sum all reaction times and divide by how many there are and multiply by 1000
          #to get average reaction time in seconds
          print('Reaction time is  ' + str(Avg) + '  seconds.')#print average reaction time 
          
          break       #break code
        












# TESTING BUTTON TURNING OFF LED -- SUCCESS
# # Create emergency buffer to store errors that are thrown inside ISR
# #micropython.alloc_emergency_exception_buf(200)


# ## Pin object to use for blinking LED. Attached to PA5
# myPin = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

# bluePin = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)


# def myCallback(timSource):
#    myPin.low() # change to command for button being hit not pi
   
   
# extint = pyb.ExtInt (bluePin,   # Which pin
#              pyb.ExtInt.IRQ_FALLING,       # Interrupt on rising edge
#              pyb.Pin.PULL_UP,             # Activate pullup resistor
#              myCallback)                   # Interrupt service routine


# while True:
#         x= urandom.randrange(2000, 3000)
#         pyb.delay(x)  # delay for 2 seconds (in milliseconds)
#         myPin.high()
#         pyb.delay(5000)
#         myPin.low()


















# isr_count = 0
# def count_isr (which_pin):        # Create an interrupt service routine
#     global isr_count
#     isr_count += 1
# extint = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
#              pyb.ExtInt.IRQ_FALLING,       # Interrupt on rising edge
#              pyb.Pin.PULL_UP,             # Activate pullup resistor
#              count_isr)                   # Interrupt service routine
# # After some events have happened on the pin...
# print (isr_count)



# # if bluePin.value ():
#     print ('Pin C1 is high. Is that legal now?')
