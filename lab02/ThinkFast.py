#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 13:44:08 2021

@author: ashleyhumpal
"""

# Import any required modules
import pyb
import micropython
import urandom

# Create emergency buffer to store errors that are thrown inside ISR
micropython.alloc_emergency_exception_buf(200)


## Pin object to use for blinking LED. Attached to PA5
myPin = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

# pin 13 -- when input triggered - use call back 
#bluePin = pyb.Pin(pyb.Pin.board.P13, mode=pyb.Pin.OUT_PP)
bluePin = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN_PP)

def myCallback(timSource):
   currentposition= timSource.counter()  # current = endtime 
   myPin.low() # change to command for button being hit not pin low -- where to find documentation
   
   
## Create a timer object (timer 6) in general purpose counting mod at 10 Hz
#myTimer = pyb.Timer(2, prescaler=79, period=999, callback=myCallback) 
myTimer = pyb.Timer(2, prescaler=79, period=0x7FFFFFFF,callback=myCallback)
#changed from 6 to 2
period=period=0x7FFFFFFF


## Callback function to run when timer overflows
#  timSource is the timer object triggering the callback



        #Timedif= endtime-starttime
#    myPin.value(1-myPin.value())  I DONT GET WHY THIS IS IN CALLBACK 

isr_count = 0
def count_isr (which_pin):        # Create an interrupt service routine
    global isr_count
    isr_count += 1
extint = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
             pyb.ExtInt.IRQ_FALLING,       # Interrupt on rising edge
             pyb.Pin.PULL_UP,             # Activate pullup resistor
             count_isr)                   # Interrupt service routine
# After some events have happened on the pin...
print (isr_count)

if bluePin.value ():
    print ('Pin C1 is high. Is that legal now?')




	                            
def update(currentposition , previousposition): 
    '''
    @brief      Updates the position of the encoder by adding good delta values
    '''
## The encoder object for the current position is created
#   currentposition= self.tim.counter #current position
    delta=currentposition-previousposition
    previousposition=currentposition
    deltamag=abs(delta)

    if deltamag> .5*(period): 
        if delta<0:
           update= delta +period
           updateddelta=update
           
        elif delta>0:
            update = delta - period
            updateddelta=update
    else:
        pass
    #position= position+ updateddelta
    
 


while True:
    try:
        print('In while loop')
        x= urandom.randrange(2, 3)
        pyb.delay(x)  # delay for 2 seconds (in milliseconds)
        previousposition= timSource.counter()
        myPin.high()
        if bluePin.high() :
            myCallback()
            update() 
            print(updateddelta)
        elif timSource.counter > previousposition + 1000:
            myPin.low()
        else:
            pass
    
            
             # nested try -all code i wanna run - not neccesary 
             
             # except- whatever here is what code will do 
    
        
        
    except KeyboardInterrupt:
        
        # calculate average time
        # display avg time
        break
        # if myPin.high():
        #     myCallback()
                
        
        
        
        
# def Timedif(starttime, endtime):
#     difference=endtime - starttime
# #    myCallback.endtime - 
# #-calll back end time and start    differnec e




# while True:
#     """Implement FSM using a while loop and an if statement
#     will run eternally until user presses CTRL-C
#     """
    
#     if state == 0: #if this state then do following
#         """perform state 0 operations
#         this init state, initializes the FSM itself, with all the
#         code features already set up
#         """
#         pyb.delay(2000)
#         state = 1
        
        
#     elif state == 1:#if th
#         """perform state 1 operations 
#         """
#         try:
#                 myPin.high()
#                 starttime= timSource.counter()
#                 pyb.delay(1000)
                
#                 if myPin.high():
#                     myCallback()
                
	
    
    

    
    