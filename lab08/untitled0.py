#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file Motor_Driver.py

This file serves as class file for the motor driver class in
Python. It will implement different methods to drive the motor.

This script has code for the motor driver class that utilizes pins, channels, and timer. 

Created on Thu Nov 12 14:09:17 2020

@author Ashley Humpal
"""

import pyb
class MotorDriver:
    '''
    @brief      A class to control the motor driver. 
    @details    This class implements a motor driver for the  ME405 board. 
    '''
    
    
    def __init__(self, nSLEEP_pin, pin_1, pin_1channel, pin_2, pin_2channel, timerval):
        
        '''
        @brief              Creates Motor objects.
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin to power the board.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param tim3         A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin. 
        '''  
        #print ('Creating a motor driver.')
        #print ('Please set nSleep_pin, pin_1, pin_1channel, pin_2, pin_2channel, and timerval.')
        
        
## The motor driver object for the pin allowing power flow      
        self.nSLEEP_pin= nSLEEP_pin
        
## The motor driver object for a pin_1 to connect to motor 1           
        self.pin_1 =pin_1
## The motor driver object for a pin_1channel to connect the correct channel to pin_1         
        self.pin_1channel= pin_1channel
## The motor driver object for a pin_2 to connect to motor 1            
        self.pin_2 = pin_2 
## The motor driver object for a pin_2channel to connect the correct channel to pin_2 
        self.pin_2channel= pin_2channel
## The motor driver object for the Timer value 
        self.timerval = timerval
## Intermediate variable to format Timer
        self.tim= pyb.Timer(self.timerval, freq=20000)
## Intermediate variable to format nSLEEP.pin
        self.Powerpin= pyb.Pin (self.nSLEEP_pin, pyb.Pin.OUT_PP);      
## Intermediate variable to format pin_1       
        self.IN1_pin = pyb.Pin (self.pin_1)
## Intermediate variable to format pin_2          
        self.IN2_pin = pyb.Pin (self.pin_2)     
## Intermediate variable to connect pin_1 with timer channel          
        self.Pinone= self.tim.channel(self.pin_1channel,pyb.Timer.PWM,pin=self.IN1_pin );
## Intermediate variable to connect pin_2 with timer channel         
        self.Pintwo= self.tim.channel(self.pin_2channel,pyb.Timer.PWM,pin=self.IN2_pin );
 
            
        

    def enable (self):
        '''
        @brief      Activates power flow by turning on nSLEEP_pin
        '''
        self.Powerpin.high()
        #print ('Enabling Motor')
        
    def disable (self):
        '''
        @brief      Disables power flow by turning off nSLEEP_pin
        '''
        self.Powerpin.low()
        #print ('Disabling Motor')
        
    def set_duty (self, duty):
        '''
        @brief      This method sets the duty cycle to be sent to the motor to the given level. Positive values cause effort in one direction, negative values in the opposite direction. 
        @param duty A signed integer holding the duty cycle of the PWM signal sent to the motor 
        '''
    
        if duty>0 :#if positive duty cycle
            self.Pintwo.pulse_width_percent(0) #set Pintwo to 0
            self.Pinone.pulse_width_percent(duty) #set Pinone to duty value
            #print('Motor going forward')
        elif duty<0: #if duty cycle negative, reverse direction
            self.Pinone.pulse_width_percent(0) #set Pinone to 0
            self.Pintwo.pulse_width_percent(abs(duty)) #set Pintwo to magnitude of duty cycle
            #print ('Motor going backward')
        else: #otherwise
            self.Pinone.pulse_width_percent(0) #set Pinone to 0
            self.Pintwo.pulse_width_percent(0) #set Pintwo to 0
            #print ('Duty cycle set to 0 for both pins. Please make sure only numerical values are input. ')
            
        
# if __name__ == '__main__':
#       from Encoderclassnew import encoder 
    
     
# ## motor 1 object created 
#       moe1= MotorDriver( pyb.Pin.cpu.A15, pyb.Pin.cpu.B4, 1, pyb.Pin.cpu.B5, 2, 3)
#       moe1.enable()
#       moe1.set_duty(35)
     # myenc=encoder()
      #moe1.set_duty(0)
#       while True:
#           myenc.update()
#           myenc.getposition()
#           print(myenc.position)
         
 
        
    
