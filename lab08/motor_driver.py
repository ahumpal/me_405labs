#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file motor_driver.py

@brief       Code to serve as a motor driver to configure the motor pins and call methods.
@details     This class file includes methods for enabling and disabling the motor,
             setting the motor duty cycle, and detecting and clearing faults. 
             When a fault is detected, the code asks for the user to input "G" to clear the fault. 
             
             Note that to test with the blue button rather than actual fault pin as we did comment lines 86-89 and uncomment lines 92-95. 
          
             Link to Source Code: https://bitbucket.org/ahumpal/me_405labs/src/master/lab08/motor_driver.py
                 
@author      Ashley Humpal and Keanau Robin
@copyright   License Info
@date        March 7, 2021

"""

import pyb # needed imports
import micropython
micropython.alloc_emergency_exception_buf(200) #for troubleshooting


class MotorDriver:
    '''
    @brief      A class to control the motor driver. 
    @details    This class implements a motor driver for the  ME405 board and contains methods for 
                enabling, disabling, setting duty cyle, and fault detection. 
    '''
# Variable for class attribute to represent whether fault has been detected.  
    faultstatus = 0 
    def __init__(self, nSLEEP_pin, pin_1, pin_1channel, pin_2, pin_2channel, timerval):
        
        '''
        @brief                 Creates Motor objects.
        @param nSLEEP_pin      A pyb.Pin object to use as the enable pin to power the board.
        @param pin_1           A pyb.Pin object to use as the input to half bridge 1.
        @param pin_1channel    A pyb.Pin object to use as the channel input for half bridge 1.
        @param pin_2           A pyb.Pin object to use as the enable pin to power the board.
        @param pin_2channel    A pyb.Pin object to use as the channel input for half bridge 2.
        @param timerval        A pyb.Timer object to use for PWM generation on pin_1 and pin_2. 
        
        '''  
        
        
        
## The motor driver object for the pin allowing power flow      
        self.nSLEEP_pin= nSLEEP_pin
        
## The motor driver object for a pin_1 to connect to motor 1           
        self.pin_1 =pin_1
## The motor driver object for a pin_1channel to connect the correct channel to pin_1         
        self.pin_1channel= pin_1channel
## The motor driver object for a pin_2 to connect to motor 1            
        self.pin_2 = pin_2 
## The motor driver object for a pin_2channel to connect the correct channel to pin_2 
        self.pin_2channel= pin_2channel
## The motor driver object for the Timer value 
        self.timerval = timerval
## Intermediate variable to format Timer
        self.tim= pyb.Timer(self.timerval, freq=20000)
## Intermediate variable to format nSLEEP.pin
        self.Powerpin= pyb.Pin (self.nSLEEP_pin, pyb.Pin.OUT_PP);      
## Intermediate variable to format pin_1       
        self.IN1_pin = pyb.Pin (self.pin_1)
## Intermediate variable to format pin_2          
        self.IN2_pin = pyb.Pin (self.pin_2)     
## Intermediate variable to connect pin_1 with timer channel          
        self.Pinone= self.tim.channel(self.pin_1channel,pyb.Timer.PWM,pin=self.IN1_pin );
## Intermediate variable to connect pin_2 with timer channel         
        self.Pintwo= self.tim.channel(self.pin_2channel,pyb.Timer.PWM,pin=self.IN2_pin );
        
        
        
    def myCallback(self):
       """Function called by interrupt to indicate that fault has been detected. 
       @brief          Changes status of variable faultstatus.
       @details        Makes faultstatus=1 when called.
       """    
       MotorDriver.faultstatus =1  #flag to indicate fault detected

       
# Variable configuring interrupt       
    extint = pyb.ExtInt (pyb.Pin(pyb.Pin.board.PB2, mode=pyb.Pin.IN), # Which pin                    
                  pyb.ExtInt.IRQ_FALLING,       # Interrupt on falling edge
                  pyb.Pin.PULL_UP,              # Activate pullup resistor
                  myCallback)                   # Interrupt service routine           
        
# test code for button  
    # extint = pyb.ExtInt (pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN),                   
    #               pyb.ExtInt.IRQ_FALLING,       # Interrupt on falling edge
    #               pyb.Pin.PULL_UP,              # Activate pullup resistor
    #               myCallback)                   # Interrupt service routine
        

        
     
    def faulted(self):
        """Function called by interrupt to indicate that fault has been detected. 
        @brief          Method that clears fault.
        @details        If fault detected, steps to clear fault taken, otherwise passed. 
        """    
        if MotorDriver.faultstatus==1: #if fault 
           MotorDriver.faultstatus=0 #clear fault status variable 
           print('Fault pin has been triggered. Motor has been automatically stopped. ') #error message for user
           self.disable() #disable motor 
           inv = input('Please enter "G" to clear fault and resume operation: ') #ask user for input 
           value=ord(inv) #get numerical value from input
           if value==71: #If G was pressed 
               self.enable() #enable the motor
              
             
           else:
               pass 
        else:
            pass
            
           
        
    
    def enable (self):
        '''
        @brief      Activates power flow by turning on nSLEEP_pin
        '''
        self.extint.disable() #disable interrupt bec nsleep high also triggers as found through testing
        self.Powerpin.high()
        self.extint.enable() # enable interrupt again
        
        #ExtInt.enable()
        

        
    def disable (self):
        '''
        @brief      Disables power flow by turning off nSLEEP_pin
        '''
        self.Powerpin.low()

        
    def set_duty (self, duty):
        '''
        @brief      This method sets the duty cycle to be sent to the motor to the given level. Positive values cause effort in one direction, negative values in the opposite direction. 
        @param duty A signed integer holding the duty cycle of the PWM signal sent to the motor 
        '''
        if MotorDriver.faultstatus == 0:
            if duty>0 :#if positive duty cycle
                self.Pintwo.pulse_width_percent(0) #set Pintwo to 0
                self.Pinone.pulse_width_percent(duty) #set Pinone to duty value
                #print('Motor going forward')
            elif duty<0: #if duty cycle negative, reverse direction
                self.Pinone.pulse_width_percent(0) #set Pinone to 0
                self.Pintwo.pulse_width_percent(abs(duty)) #set Pintwo to magnitude of duty cycle
                #print ('Motor going backward')
            else: #otherwise
                self.Pinone.pulse_width_percent(0) #set Pinone to 0
                self.Pintwo.pulse_width_percent(0) #set Pintwo to 0
                    #print ('Duty cycle set to 0 for both pins. Please make sure only numerical values are input. ')
        else:
            self.faulted()
            
                
        
# if __name__ == '__main__':  
# ## motor 1 object created 
#       #from encoder_driver import encoder 
#        moe1= MotorDriver( pyb.Pin.cpu.A15, pyb.Pin.cpu.B4, 1, pyb.Pin.cpu.B5, 2, 3)
      
#       #moe2= MotorDriver( pyb.Pin.cpu.A15, pyb.Pin.cpu.B0, 3, pyb.Pin.cpu.B1, 4, 3)
#        moe1.enable()
#       # moe1.set_duty(35)
#       # enc1=encoder()
#       # while True:
#       #     enc1.update()
#       #     enc1.getposition()
#       #     print(enc1.position)
#        # x=0
#        for x in range(3): 
#            moe1.set_duty(35)
#            #moe2.set_duty(35)
#            pyb.delay(5)
#            x+=1 
#        moe1.set_duty(0)
#        #moe2.set_duty(0)
#        moe1.disable()
          
      
         
     
         
 
        
    
