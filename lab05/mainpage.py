#!/usr/bin/env python3
# -*- coding: utf-8 -*-
## @file mainpage.py
# Documentation for / use of mainpage.py
# 
# Detailed doc for mainpage.py
#
# @mainpage
#
# @section sec_port Portfolio Details 
# This is my ME 405 Portfolio. See individual modules for details.  Here is the link back to the mainpage: https://ahumpal.bitbucket.io/
# 
# 
# Included modules are:
#   * Lab01 (\ref sec_lab1)
#   * Lab02 (\ref sec_lab2)  
#   * Lab03 (\ref sec_lab3) 
#   * Lab04 (\ref sec_lab4)
#
# @section sec_lab1 Lab01 Documentation
#   * Source: https://bitbucket.org/ahumpal/me_405labs/src/master/lab01/Vendotron.py
#   * Documentation: https://ahumpal.bitbucket.io/me405/Vendotron_8py.html
# @image html VendotronFSM.png