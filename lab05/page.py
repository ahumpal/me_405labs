#!/usr/bin/env python3
# -*- coding: utf-8 -*-
## @page Lab05Handcalcs
# 
# This section contains hand calculations neeeded to analyze the motion of 
# a pivoting platform. These hand calculations were made in conjuction with my
# partner Ryan Funchess. A link to his porfolio is here: https://rfunches.bitbucket.io/
#
# To begin with we were given these following parameters and figures:
#
# @image html lab5param1.png
#
# @image html lab5param2.png
#
# @image html lab5param3.png


