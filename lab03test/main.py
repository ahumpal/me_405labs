#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file main.py
@brief       Code that generates data for a Time vs Voltage Step Response for a button press.
@details     Once run, the code waits for a G from the Frontend to begin waiting for button press.
             An interrupt is triggered by the button press that the user does, and the buffer begins collecting time and voltage values that result from that press. 
             Those values are then sent back to the frontend.
             
             This code only succesfully sends data for 1 good plot at a time. If user wants another plot,
             both this file as well as the frontend have to be restarted.
            
             Link to Source Code: https://bitbucket.org/ahumpal/me_405labs/src/master/lab03test/main.py
                 
@author      Ashley Humpal
@copyright   License Info
@date        January 26, 2021
"""

import pyb#import all neccessary modules
import micropython
import utime 
import array
from pyb import USB_VCP

## Variable representing nucleo board connection to USB
myuart = USB_VCP()


# Create emergency buffer to store errors that are thrown inside ISR
micropython.alloc_emergency_exception_buf(200) #for troubleshooting

## Create a timer object (timer 6) in general purpose counting mode
myTimer = pyb.Timer(6, freq=20000)

## Pin object to use for ADC pin
ADC=pyb.ADC(pyb.Pin.board.PC0)


## Pin object to use for PC13
bluePin = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)

## Counter for appending to list
n=0

## Variable representing status of button being pressed 
button=0
## Variable representing time value in micro seconds
ti=0

## List representing time values 
timee=[]

## List representing voltage values 
volt=[]


def myCallback(timSource):
    """Function called by interrupt to set button value to 1 to indicate it has been pressed. 
    @brief          Makes button variable global and set it to 1.

    """    
  

## Button value set to global variable   
    global button #create global variable to send
    button=1 #timer counter variable set

## Variable configuring interrupt

extint = pyb.ExtInt (bluePin,                    # Which pin
              pyb.ExtInt.IRQ_FALLING,      # Interrupt on falling edge
              pyb.Pin.PULL_NONE,           # No pullup resistor activated
              myCallback)                  # Interrupt service routine

## Variable representing buffer for memory storage
buffy=array.array ('H', (0 for index in range (10000)))


while True: #if true do this 
    if  myuart.any() != False and myuart.any() != None: #if  normal key has been pressed
## Variable representing readline and decode
        crt= myuart.readline().decode('ascii') # read key 
## Variable representing key Unicode value
        Z= ord(crt) #get unicode value of key
        if Z==71:# if key is G then do following
            while True: #loop
## Intermediate list representing voltage values            
                volt=[] #set list empty
## Intermediate list representing time values    
                timee=[] #set list empty
                if button==1: #if button pressed
## Variable representing read_timed command
                    val=ADC.read_timed(buffy, myTimer) #read analog values in buffer
                    while buffy[n] <=4093: #while buffer value is less than 4093
## Variable representing raw ADC value in counts
                        v=buffy[n] #set v to buffer value
                        if v>10: #If buffer value not negligable ( so more than 10)
## Variable representing ADC values in volts
                            z=v/(4095/3.3) #convert ADC raw value to volts
                            volt.append(z) #append to list
                            ti+=50 #create time increments
                            timee.append(ti) #append
                        n+=1 #increment counter
                        if buffy[9999] < 10:#if last value in buffer is negligable 
                                ADC.read_timed(buffy, myTimer) #reset buffer
                                n=0 #reset counter
                                

                    myuart.write('{:};{:}\r\n'.format(timee, volt)) #send lists to front
                    volt=[] #empty list
                    
                    break
                else:
                    pass