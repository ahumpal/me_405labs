#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file frontend.py
@brief       Code that serves as a user interface and generates a Time vs Voltage Step Response and CSV file for a button press.
@details     Once run, the code prompts the user for a G and sends that input to the backend.
             It waits 10 seconds for the button press as well as to read the data sent from the backend.
             Once values are read, they are plotted and a CSV file is created.
             
             This code only succesfully creates 1 good plot at a time. If user wants another plot,
             both this file as well as the backend have to be restarted.
            
             Link to Source Code: https://bitbucket.org/ahumpal/me_405labs/src/master/lab03test/frontend.py
                 
@author      Ashley Humpal
@copyright   License Info
@date        January 26, 2021
"""

# import all needed modules 
import time
import serial
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
## Variable representing serial connection created
ser = serial.Serial(port='/dev/tty.usbmodem205B337657522',baudrate=115273,timeout=1)



## List representing time
TIME=[]
## List representing voltage values
VOLT=[]
    
#Function to write inputs
def sendChar():
    """Function to send user input to backend. 
   @brief          Encodes user input as ascii key and sends to backend.
   @details        Only sends ascii if G input, otherwise gives error message
   """    
    while True:
## Variable representing user input
        inv = input('Give me a character. Type "G" to have Nucleo await button press: ')
## Variable representing unicode value of key
        x= ord(inv)
        if x != 71: #if not G
            print ('Error, Non "G" character pressed. Please only press "G"')
        else: #if G then 
            print('Please press button and wait 10 secs for step response to populate.')
            ser.write(str(inv).encode('ascii')) #sends key to the board
            break

     
def receiveChar():
    """Function to read strings sent from the backend, plot values, and create CSV file. 
   @brief          Decodes values, puts them into lists as floats, then plots lists and saves them as a CSV file.
   
   """       
    time.sleep(10) #delay to give time to get data from backend
    while True:
        myval = ser.readline().decode('ascii') #decode 
       
        if myval !=0:  #if any key is read
## Intermediate variable to strip and split combined time and voltage list          
            line_list = myval.strip('[]\r\n').split('];[')	# strip any special characters and then split the string 
           
## Intermediate variable to split time list             
            TIMEs = line_list[0].split(',') # split each list at commas
## Intermediate variable to split voltage list   
            VOLTs = line_list[1].split(',')# split each list at commas
            
           
            VOLT=[float(item) for item in VOLTs] #make strings into floats
            TIME=[float(item) for item in TIMEs] #make strings into floats
            
            TIME.insert(0, 0) # insert a 0 as the first data point
            VOLT.insert(0,0) # insert a 0 as the first data point
            
           # print(TIME)
           # print(VOLT)
           
            
            plt.plot(TIME, VOLT) #plot time vs volts
 
            
            plt.xlabel('Time [microseconds]') #xlabel
            plt.ylabel('Voltage [volts]') #ylabel
            plt.title('Step Response') #title 
            
        
            ADClist = [i * (4095/3.3) for i in VOLT]
            
            
## Variable to consolidate list data
            data = list(zip(TIME,ADClist)) #zip data
    
            np.savetxt('AshleyHumpal__array.csv',data, delimiter =',')#make csv file
            
            break
   
   

if __name__ == '__main__': 
    sendChar()
    receiveChar()  
  