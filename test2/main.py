#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 14:06:21 2021

@author: ashleyhumpal
"""

from pyb import USB_VCP
myuart = USB_VCP()
myuart.isconnected()
while True:
     if myuart.any() != False and myuart.any() != None:
         val = myuart.readline()
         myuart.write('You sent an ASCII '+ str(val) +' to the Nucleo')
        