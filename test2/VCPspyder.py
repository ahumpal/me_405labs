#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 14:03:23 2021

@author: ashleyhumpal
"""
import time
import serial
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
ser = serial.Serial(port='/dev/tty.usbmodem205B337657522',baudrate=115273,timeout=1)

## Empty list representing position
x=[]
## Empty list representing time
t=[]

    
#Function to write inputs

def sendChar():
    while True:
## Variable representing user input
        inv = input('Give me a character. Type "G" to have Nucleo begin sampling from the encoder. Type "S" to stop data collection at anytime. ')
        x= ord(inv)
        if x != 71:
            print ('Error, Non "G" character pressed. Please only press "G"')
        else:
            print('gonna encodeeee')
            ser.write(str(inv).encode('ascii')) #sends it to the board
            break

     
def receiveChar():
    time.sleep(6)
    while True:
        myval = ser.readline().decode('ascii')
        #print(myval)
        if myval !=0:  #if any key is read
            line_list = myval.strip('[]\r\n').split(',')	# strip any special characters and then split the string into wherever a comma appears;
            #print(line_list)
            k=len(line_list)
            #print(k)
            ti=0
            for indx in range(k):
                ti+=50
                t.append(ti)
#         #     print(buffy)
            t.insert(0, 0)
            line_list.insert(0, 0)
            print('Time')
            print(t)
            print('voltage')
            print(line_list)
            
            #plt.plot(t, line_list)
            #plt.plot(t, line_list, 'o', color='black');
            plt.plot(t, line_list, 'o', color='black');
            
            plt.xlabel('Time [ms]')
            plt.ylabel('ADC [volts]')
            # data = list(zip(t,line_list))
    
            # np.savetxt('AshleyHumpal__array.csv',data, delimiter =',')  
            #print(line_list)
            break
        
        #     t.append(int(line_list[0])) #append values to list t
        #     x.append(int(line_list[1])) #append values to list x

    
           
      
        # else :
        #     pass
      
  
  
    # plt.plot(t, x)
    # plt.xlabel('Time [s]')
    # plt.ylabel('Position [Degrees]')
    # data = list(zip(t,x))
    
    # np.savetxt('AshleyHumpal_array.csv',data, delimiter =',')  
   

if __name__ == '__main__': 
    sendChar()
    receiveChar()  
  
  
  
# ser.close()  
  









#Working test 
# import serial
# ser = serial.Serial(port='/dev/tty.usbmodem205B337657522',baudrate=115273,timeout=1)

# def sendChar():
#     inv = input('Give me a character: ')
#     ser.write(str(inv).encode('ascii'))
#     myval = ser.readline().decode('ascii')
#     #print(myval)
#     return myval

# for n in range(1):
#     print(sendChar())
    
# ser.close()

