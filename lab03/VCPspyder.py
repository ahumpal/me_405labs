#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 14:03:23 2021

@author: ashleyhumpal
"""

import serial
ser = serial.Serial(port='/dev/tty.usbmodem205B337657522',baudrate=115273,timeout=1)

def sendChar():
    inv = input('Give me a character: ')
    ser.write(str(inv).encode('ascii'))
    myval = ser.readline().decode('ascii')
    return myval

for n in range(1):
    print(sendChar())
    
ser.close()
