#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file ADCbackend.py
@brief       Code for button push test to record user reaction time when seeing lit LED. 
@details     Once run, the code waits 2-3 seconds and lights LED. It gives the user 1 second 
             to press button PC13 and records that counter time. It utilizes an interrupt mechanism when the button is pushed. It gives an error message if 
             user doesn't push the button within the interval and averages all reaction times after cntrl C is pressed.
            
             Link to Source Code: https://bitbucket.org/ahumpal/me_405labs/src/master/lab02/T_F_Test.py
                 
@author      Ashley Humpal
@copyright   License Info
@date        January 23, 2021
"""


# Import any required modules
import pyb
import micropython

import utime 
from pyb import UART

## Variable representing nucleo board connection
myuart=UART(2)
## Variable representing myuart
ser=myuart
#importing neccessary information

# Create emergency buffer to store errors that are thrown inside ISR
micropython.alloc_emergency_exception_buf(200) #for troubleshooting



## List representing reaction time 
times=[] 

## Timer variable initialization 
t=0

## Pin object to use for ADC pin
myPin = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

## Pin object to use for PC13
bluePin = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)

## Create a timer object (timer 2) in general purpose counting mode
myTimer = pyb.Timer(2, prescaler=79, period=0x7FFFFFFF)

def myCallback(timSource):
   """Function called by interrupt to record timer count value and turn off LED. 
   @brief          Turns LED off and sets timer counter value.
   @details        Makes counter value a global variable
   """    
   myPin.low() #turn off LED

## Timer counter value set to global variable   
   global t #create global variable to send
   t=myTimer.counter() #timer counter variable created
   
   
   
## Variable configuring interrupt

extint = pyb.ExtInt (bluePin,                    # Which pin
             pyb.ExtInt.IRQ_FALLING,       # Interrupt on falling edge
             pyb.Pin.PULL_UP,              # Activate pullup resistor
             myCallback)                   # Interrupt service routine




## New class called collecting
class ADCcollecting: 
    '''
    @brief      A finite state machine to control the data collection of the encoder.
    @details    This class implements a finite state machine to control the
                sampling of time and position data points from the encoder.
    '''
    
## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
## Constant defining State 1
    S1_WAITING_USER     = 1    
    
## Constant defining State 2
    S2_COLLECTING     = 2 

## Constant defining State 2
    S2_COLLECTING     = 2 
    
## Constant defining State 2
    S2_COLLECTING     = 2 
  
    
      
    def __init__(self, encoder, interval):
        '''
        @brief          Creates an encoder object.
        
        '''
        
## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
## Intializes the encoder.
        
        self.encoder=encoder
        

## Counter that describes the number of times the task has run
        self.runs = 0
        
##  The amount of time in milliseconds between runs of the task
        self.interval= int(interval*1e3)
        
## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

## An empty list representing position
        self.x=[]

## An empty list representing time       
        self.t=[]
        
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
## Variable representing current time
        self.curr_time = utime.ticks_ms() #sets current time
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0): #condition for state
            if(self.state == self.S0_INIT): 
                self.transitionTo(self.S1_WAITING_USER) #begins transition
            #0 to 1 here
            
            
            elif(self.state == self.S1_WAITING_USER): #if at state 1 do following 
                if myuart.any() !=0: #if key pressed 
                    crt= ser.readchar() #variable to read key 
                    if crt==71:# if key is g then do following
                        self.transitionTo(self.S2_COLLECTING) #state transition
                    else :
                        #print('Please only type "G" to start data collection or "S" to stop data collection')
                        self.transitionTo(self.S1_WAITING_USER)   #any other key then transition back to state 1
                        
                        
            elif(self.state == self.S2_COLLECTING): #if state 2 then do following
                       self.encoder.update()#update position
                       self.encoder.getposition()#get position value
                       ser.write('{:},{:}'.format (self.t, self.x))
                       if (self.interval*self.runs>=10000): #if 10 seconds have passed do following
                               self.t.clear()#clear time list
                               self.runs=-1;#reset counter
                               #print('Time interval reached')
                               self.transitionTo(self.S1_WAITING_USER)
                              # self.x.clear()
                               #self.transitionTo(self.S3_NOTHING) #transition to state 3
                       elif ser.readchar() ==83:#s
                            
                            self.t.clear()
                            self.runs=-1;
                            self.transitionTo(self.S1_WAITING_USER)
                            #self.x.clear()
                            #self.transitionTo(self. S3_NOTHING )
                       else :
                           self.transitionTo(self. S2_COLLECTING )

                       self.runs += 1
                
            
                       
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState 
         
