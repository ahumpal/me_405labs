#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 14:06:21 2021

@author: ashleyhumpal
"""




# Import any required modules
import pyb
import micropython
import utime 
#from pyb import UART
import array
from pyb import USB_VCP
myuart = USB_VCP()
## Variable representing nucleo board connection
#myuart=UART(2)
## Variable representing myuart
#ser=myuart
#importing neccessary information

# Create emergency buffer to store errors that are thrown inside ISR
micropython.alloc_emergency_exception_buf(200) #for troubleshooting

#myTimer = pyb.Timer(2, freq=200000)

myTimer = pyb.Timer(6, freq=20000)

## Pin object to use for ADC pin
#ADC=pyb.ADC(pyb.Pin.board.A0)

ADC=pyb.ADC(pyb.Pin.board.PC0)
# a5

## Pin object to use for PC13
bluePin = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)

## Create a timer object (timer 2) in general purpose counting mode
n=0
button=0


volt=[]


def myCallback(timSource):
    """Function called by interrupt to record timer count value and turn off LED. 
    @brief          Turns LED off and sets timer counter value.
    @details        Makes counter value a global variable
    """    
  

## Timer counter value set to global variable   
    global button #create global variable to send
    button=1 #timer counter variable created
   
    # global t #create global variable to send
    # t=myTimer.counter() #timer counter variable created
   
   
   
## Variable configuring interrupt

extint = pyb.ExtInt (bluePin,                    # Which pin
              pyb.ExtInt.IRQ_FALLING,       # Interrupt on falling edge
              pyb.Pin.PULL_NONE,              # Activate pullup resistor
              myCallback)                   # Interrupt service routine
#PULL_NONE?
buffy=array.array ('H', (0 for index in range (10000)))



while True:
    if  myuart.any() != False and myuart.any() != None:
       
        crt= myuart.readline().decode('ascii') #variable to read key 
        #print('readline IN DA BACK')
        #print(crt)
        Z= ord(crt)
        #print(Z)
        if Z==71:# if key is G then do following
            #print('we made it to Z')
            
            #print('G has been pressed in da back')
        
    
            while True:
                volt=[]
                
                if button==1:
                    #print('button has been pressed sis')
                    val=ADC.read_timed(buffy, myTimer)
                    
                
                    while buffy[n] <=4093:
                        #t=myTimer.counter() #timer counter variable created
                        #t=ti[idx]
                        #time.append(t)
                        #print('buffer set up')
                        v=buffy[n]
                        if v>10:
                            z=v/(4095/3.3)
                            volt.append(z)
                            
                            
                            #print('v append')
                            #print(v)
                        n+=1
                       
                        if buffy[9999] < 10:
                                #print('buff reset')
                                ADC.read_timed(buffy, myTimer)
                                n=0
                                
            
                    
                    #print(volt)
                    myuart.write(str(volt))
                    volt=[]
                    
                    break
                else:
                    pass

#print(volt)

#myuart.write(str(volt))





# # Import any required modules
# import pyb
# import micropython
# import utime 
# #from pyb import UART
# import array
# from pyb import USB_VCP
# myuart = USB_VCP()
# ## Variable representing nucleo board connection
# #myuart=UART(2)
# ## Variable representing myuart
# #ser=myuart
# #importing neccessary information

# # Create emergency buffer to store errors that are thrown inside ISR
# micropython.alloc_emergency_exception_buf(200) #for troubleshooting

# #myTimer = pyb.Timer(2, freq=200000)

# myTimer = pyb.Timer(6, freq=20000)

# ## Pin object to use for ADC pin
# #ADC=pyb.ADC(pyb.Pin.board.A0)

# ADC=pyb.ADC(pyb.Pin.board.PC0)
# # a5

# ## Pin object to use for PC13
# bluePin = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)

# ## Create a timer object (timer 2) in general purpose counting mode
# n=0
# button=0


# volt=[]


# def myCallback(timSource):
#     """Function called by interrupt to record timer count value and turn off LED. 
#     @brief          Turns LED off and sets timer counter value.
#     @details        Makes counter value a global variable
#     """    
  

# ## Timer counter value set to global variable   
#     global button #create global variable to send
#     button=1 #timer counter variable created
   
#     # global t #create global variable to send
#     # t=myTimer.counter() #timer counter variable created
   
   
   
# ## Variable configuring interrupt

# extint = pyb.ExtInt (bluePin,                    # Which pin
#               pyb.ExtInt.IRQ_FALLING,       # Interrupt on falling edge
#               pyb.Pin.PULL_NONE,              # Activate pullup resistor
#               myCallback)                   # Interrupt service routine
# #PULL_NONE?
# buffy=array.array ('H', (0 for index in range (10000)))



# while True:
#     if button==1:
#         val=ADC.read_timed(buffy, myTimer)
        
    
#         while buffy[n] <=4093:
#             #t=myTimer.counter() #timer counter variable created
#             #t=ti[idx]
#             #time.append(t)
#             v=buffy[n]
#             if v>0:
#                 volt.append(v)
#             n+=1
           
#             if buffy[9999] < 10:
#                     ADC.read_timed(buffy, myTimer)
#                     n=0
                    

        
        
        
#         break
#     else:
#         pass

# #print(volt)
# myuart.write(str(volt))











# from pyb import USB_VCP
# myuart = USB_VCP()
# while True:
#      if myuart.any() != 0 and myuart.any() != None:
#          val = myuart.readline()
#          myuart.write('You sent an ASCII '+ str(val) +' to the Nucleo')



# # Import any required modules
# import pyb
# import micropython
# import utime 
# from pyb import UART
# import array

# ## Variable representing nucleo board connection
# myuart=UART(2)
# ## Variable representing myuart
# ser=myuart
# #importing neccessary information

# # Create emergency buffer to store errors that are thrown inside ISR
# micropython.alloc_emergency_exception_buf(200) #for troubleshooting

# #myTimer = pyb.Timer(2, freq=200000)

# myTimer = pyb.Timer(6, freq=200)

# ## Pin object to use for ADC pin
# #ADC=pyb.ADC(pyb.Pin.board.A0)

# ADC=pyb.ADC(pyb.Pin.board.PC0)
# # a5

# ## Pin object to use for PC13
# bluePin = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)

# ## Create a timer object (timer 2) in general purpose counting mode

# button=0
# time=[]
# volt=[]
# def myCallback(timSource):
#    """Function called by interrupt to record timer count value and turn off LED. 
#    @brief          Turns LED off and sets timer counter value.
#    @details        Makes counter value a global variable
#    """    
  

# ## Timer counter value set to global variable   
#    global button #create global variable to send
#    button=1 #timer counter variable created
   
#    # global t #create global variable to send
#    # t=myTimer.counter() #timer counter variable created
   
   
   
# ## Variable configuring interrupt

# extint = pyb.ExtInt (bluePin,                    # Which pin
#              pyb.ExtInt.IRQ_FALLING,       # Interrupt on falling edge
#              pyb.Pin.PULL_NONE,              # Activate pullup resistor
#              myCallback)                   # Interrupt service routine
# #PULL_NONE?
# buffy=array.array ('H', (0 for index in range (100)))



# while True:
#     if button==1:
#         val=ADC.read_timed(buffy, myTimer)
#         for idx in range(len(buffy)):
#             ti=myTimer.counter() #timer counter variable created
#             #t=ti[idx]
#             time.append(ti)
#             v=buffy[idx]
#             volt.append(v)
            
#             print('{:},{:}'.format (time, volt))
#            # print('{:},{:}'.format(idx, buffy[idx]))
        
#         # for n in range(len(buffy)):
#         #     print(buffy)
#         break
#     else:
#         pass
    
#         #for n in range(len(buffy)):
#             #print('{:},{:}'.format(n, buffy[n]))
        



# #ADC TESTING CODE SUCCESS

# # Import any required modules
# import pyb
# import micropython
# import utime 
# from pyb import UART
# import array

# ## Variable representing nucleo board connection
# myuart=UART(2)
# ## Variable representing myuart
# ser=myuart
# #importing neccessary information

# # Create emergency buffer to store errors that are thrown inside ISR
# micropython.alloc_emergency_exception_buf(200) #for troubleshooting

# #myTimer = pyb.Timer(2, freq=200000)

# myTimer = pyb.Timer(6, freq=20000)

# ## Pin object to use for ADC pin
# #ADC=pyb.ADC(pyb.Pin.board.A0)

# ADC=pyb.ADC(pyb.Pin.board.PC0)
# # a5



# ## Pin object to use for PC13
# bluePin = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)

# ## Create a timer object (timer 2) in general purpose counting mode

# button=0
# def myCallback(timSource):
#     """Function called by interrupt to record timer count value and turn off LED. 
#     @brief          Turns LED off and sets timer counter value.
#     @details        Makes counter value a global variable
#     """    
  

# ## Timer counter value set to global variable   
#     global button #create global variable to send
#     button=1 #timer counter variable created
   
#     # global t #create global variable to send
#     # t=myTimer.counter() #timer counter variable created
   
   
   
# ## Variable configuring interrupt

# extint = pyb.ExtInt (bluePin,                    # Which pin
#               pyb.ExtInt.IRQ_FALLING,       # Interrupt on falling edge
#               pyb.Pin.PULL_NONE,              # Activate pullup resistor
#               myCallback)                   # Interrupt service routine
# #PULL_NONE?
# buffy=array.array ('H', (0 for index in range (10000)))



# while True:
#     if button==1:
#         val=ADC.read_timed(buffy, myTimer)
#         for n in range(len(buffy)):
#             print('{:},{:}'.format(n, buffy[n]))
        
#         # for n in range(len(buffy)):
#         #     print(buffy)
#         break
#     else:
#         pass


# CODE WITH SERIAL STUFF
# while True:
#      if myuart.any() !=0: #if key pressed 
#          crt= ser.readchar() #variable to read key 
#          if crt==71:# if key is g then do following
#             if button==1:
#                 val=ADC.read_timed(buffy, myTimer)
#                 for n in range(len(buffy)):
#                     print('{:},{:}'.format(n, buffy[n]))
                
#                 # for n in range(len(buffy)):
#                 #     print(buffy)
#             break
#      else:
#         pass