#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 12:44:40 2021

@author: ashleyhumpal
"""

"""Initialize code to run FSM (not the init state). This code initializes the9program, not the FSM
"""
state = 0


def getChange():
    """ Computes change
    """
    pass

def printWelcome():
    """Prints a VendotronˆTMˆ welcome message with beverage prices
    """
    
while True:
    """Implement FSM using a while loop and an if statement
    will run eternally until user presses CTRL-C
    """
    
    if state == 0:
        """perform state 0 operations
        this init state, initializes the FSM itself, with all the
        code features already set up
        """
        printWelcome()
        state = 1
        
    elif state == 1:
        """perform state 1 operations
        """
        
        state = 2# s1 -> s2
        
    elif state == 2:
        """perform state 2 operations
        """
        state = 3# s2 -> s3
        
    elif state == 3:
        """perform state 2 operations
        """
        state = 1# s3 -> s1
        
        #update balance 
        
        
        
        
        
        
    