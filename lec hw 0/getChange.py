#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This file computes correct change for a given
purchase. The purchase price is in numerical form while the payment is in tuple form.
The change is returned in tuple form. 

Created on Thu Jan  7 16:26:39 2021

@author Ashley Humpal
"""


def getChange(price, payment):
    
    if len(payment)!= 8: # if tuple is not correct length 
        #print('Payment not in correct tuple form') #print this
        return None
        
    else: #otherwise
        penny=payment[0]*.01  #set value of specific position in tuple
        nickel=payment[1]*.5  #set value of specific position in tuple
        dime=payment[2]*.10   #set value of specific position in tuple
        quarter=payment[3]*.25 #set value of specific position in tuple
        one=payment[4]*1      #set value of specific position in tuple
        five=payment[5]*5     #set value of specific position in tuple
        ten =payment[6]*10    #set value of specific position in tuple
        twenty=payment[7]*20  #set value of specific position in tuple
        
        #print('Price of item is '+str(price) + ' dollars.')
        numericalpayment= penny + nickel + dime + quarter + one + five + ten + twenty #calculate total payment in numerical form
        
        #print('Payment given in numerical form is ' +str(numericalpayment)+ ' dollars.')
        numericalchange= round(numericalpayment- price ,2) #calculate difference between price and payment
        
        if numericalchange < 0: #if payment is less then price print message
            #print('Insufficient Funds')
            return None
        else:
            
            change20 = numericalchange // 20  #calculate how many 20's in change
            newtot= numericalchange- 20*(change20) #update total without the 20's
            
            change10= newtot//10 #calculate how many 20's in change
            newtot1=newtot- 10*(change10) #total updated
            
            change5= newtot1//5 #calculate how many 5's in change
            newtot2=newtot1- 5*(change5) #total updated
            
            change1= newtot2//1 #calculate how many 1's in change
            newtot3=newtot2- 1*(change1) #total updated
            
            change25 = newtot3//.25 #calculate how many quarters in change
            newtot4=newtot3- .25*(change25) #total updated
            
            changedime= newtot4//.10 #calculate how many dimes in change
            newtot5=newtot4- .10*(changedime) #total updated
            
            changenickel = newtot5//.05 #calculate how many nickels in change
            newtot6=newtot5- .05*(changenickel) #total updated
            
            changepenny = newtot6 // .01 #calculate how many pennies in change
            newtot7=newtot6- .01*(changepenny) #total updated
        
        
            change= (changepenny, changenickel, changedime, change25, change1, change5, change10, change20)
            print(change)
            
            
            
   
if __name__ == '__main__':
    
    PriceInput= 21.50
    PaymentInput= (0, 0, 0, 0, 2, 0, 0, 1)
    getChange(PriceInput, PaymentInput)
    
    
    
    
    
    
    
    
    # PriceInput= 27.24
    # PaymentInput= (3, 0, 0, 2, 1, 0, 0, 1)
    # getChange(PriceInput, PaymentInput)



# def getChange(price, payment):
#     PriceInput=input('Price in numerical form')
#     PaymentInput=input('Give payment being given in tuple form')
#     price=int(PriceInput)
#     payment1=(payment)
#     penny=payment1[0]*.1 
#     nickel=payment1[1]*.5 
#     dime=payment1[2]*.10 
#     quarter=payment1[3]*.25
#     one=payment1[4]*1 
#     five=payment1[5]*5 
#     ten =payment1[6]*10 
#     twenty=payment1[7]*20 
#     numericalpayment= penny + nickel + dime + quarter + one + five + ten + twenty 
#     print(numericalpayment)
  
# PriceInput= input('Enter price in numerical form:')
# PaymentInput= tuple(input('Give payment in tuple form without paranthesese like so: pennies, nickles, dimes, quarters, ones, fives, tens, and twenties: ').split(','))


# def getChange(price, payment):
#     price= float(PriceInput)
#     payment=(PaymentInput)
  
#     penny=float(payment[0])*.01 
#     nickel=float(payment[1])*.05 
#     dime=float(payment[2])*.10 
#     quarter=float(payment[3])*.25
#     one=float(payment[4])*1 
#     five=float(payment[5])*5 
#     ten =float(payment[6])*10 
#     twenty=float(payment[7])*20 
#     print('Price of item is '+str(price) + ' dollars.')
#     numericalpayment= penny + nickel + dime + quarter + one + five + ten + twenty 
#     print('Payment given in numerical form is ' +str(numericalpayment)+ ' dollars.')
#     numericalchange= round(numericalpayment- price , 2)
#     print('Change to be expected in numerical form is ' +str(numericalchange)+ ' dollars.')
  
    
# getChange(PriceInput, PaymentInput)

  



      