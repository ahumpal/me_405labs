#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 14:48:56 2021

@author: ashleyhumpal
"""

from pynput import keyboard

def on_press(key):
    try:
        print('alphanumeric key {0} pressed'.format(
            key.char))
    except AttributeError:
        print('special key {0} pressed'.format(
            key))

def on_release(key):
    print('{0} released'.format(
        key))
    if key == keyboard.Key.esc:
        # Stop listener
        return False

# Collect events until released
with keyboard.Listener(
        on_press=on_press,
        on_release=on_release) as listener:
    listener.join()

# ...or, in a non-blocking fashion:
listener = keyboard.Listener(
    on_press=on_press,
    on_release=on_release)
listener.start()




# #!/usr/bin/env python3
# # -*- coding: utf-8 -*-
# """
# @file Vendotron.py
# @brief       FSM for Vending machine Vendotron
# @details     This File takes in coins entered by the user and lets them select a beverage. It checkes if the payment is enough for the price of the beverage and if so 
#              it drops the beverage and ejects the remaining balance. 
# @author      Ashley Humpal
# @copyright   License Info
# @date        January 12, 2021
# """

# """Initialize code to run FSM (not the init state). This code initializes the program, not the FSM
# """
# ## The current balance in the Vendotron in cents
# Drink_balance=0
# ## The first state of the FSM
# state = 0


# import keyboard

# ## Creation of variable for Key press
# pushed_key = None

# def on_keypress (thing):
#     """ Callback which runs when the user presses a key.  
#     """
#     global pushed_key

#     pushed_key = thing.name

# keyboard.on_press (on_keypress)  ########## Set callback



# def getChange(price, payment):
#     """ Computes change by taking the difference between a given price and payment given, both in numerical form. Returns change in a 8 length tuple ranging from the amount of pennies needed all the way to the number of $20 bills.
#     @brief           Computes change for monetary transaction
#     @details         Computes change given set of bills/coins and returns denominations
#     @param price     The price of the item as a numerical value in dollars
#     @param payment   The payment given for item as a numerical value in dollars
    
#     """

#     numericalchange= payment - price
#     change20 = numericalchange // 2000  #calculate how many 20's in change
#     newtot= numericalchange- 2000*(change20) #update total without the 20's
    
#     change10= newtot//1000 #calculate how many 20's in change
#     newtot1=newtot- 1000*(change10) #total updated
    
#     change5= newtot1//500 #calculate how many 5's in change
#     newtot2=newtot1- 500*(change5) #total updated
    
#     change1= newtot2//100 #calculate how many 1's in change
#     newtot3=newtot2- 100*(change1) #total updated
    
#     change25 = newtot3//25 #calculate how many quarters in change
#     newtot4=newtot3- 25*(change25) #total updated
    
#     changedime= newtot4//10 #calculate how many dimes in change
#     newtot5=newtot4- 10*(changedime) #total updated
    
#     changenickel = newtot5//5 #calculate how many nickels in change
#     newtot6=newtot5- 5*(changenickel) #total updated
    
#     changepenny = newtot6 // 1 #calculate how many pennies in change
#     newtot7=newtot6- 1*(changepenny) #total updated

#     change= (changepenny, changenickel, changedime, change25, change1, change5, change10, change20)
#     return change    

# def printWelcome():
#     """Prints a VendotronˆTMˆ welcome message with beverage selctions and prices. 
#     @brief          Welcomes user to Vendotron
#     @details        Gives instruction on how to enter certain bills and how to call for certain beverages
#     """
#     print('Welcome to VendotronˆTM. To begin with please insert change.')
#     print('''
#           Press 0 to add a penny,
#           1 for nickel, 
#           2 for dime,
#           3 for quarter,
#           4 for $1,
#           5 for $5,
#           6 for $10,
#           and 7 for $20.
#           ''')
#     print('After inserting money, please select a drink.')
#     print('''
#           Press C for cuke, $1;
#           P for Popsi, $1.25;
#           S for Spryte, $1.50;
#           D for Dr. Pupper, $1.75''' )
#     print(' To eject balance at any time please press E')
#     print('Thank You!!!')
    
          


# while True:
#     """Implement FSM using a while loop and an if statement
#     will run eternally until user presses CTRL-C
#     """
    
#     if state == 0:
#         """perform state 0 operations
#         this init state, initializes the FSM itself, with all the
#         code features already set up
#         """
#         printWelcome() #calls method
#         state = 1 #transitions state
#     elif state == 1: #if state 1
#         """perform state 1 operations 
#         """
#         try:
#         #which is adding and displaying balance.
        
#         #try and accept block 
#             if pushed_key:#pushed_key method
#                 if pushed_key == "0":
#                     Drink_balance=  Drink_balance + 1 #adds value in cents 
# ## The current balance in the Vendotron in dollars                   
#                     Drink_balance1= Drink_balance/100
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1
#                 elif pushed_key == '1':
#                     Drink_balance=  Drink_balance + 5#adds value in cents
#                     Drink_balance1= Drink_balance/100
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1 #state transition
#                 elif pushed_key == '2':
#                     Drink_balance=  Drink_balance + 10#adds value in cents
#                     Drink_balance1= Drink_balance/100
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1 #state transition
#                 elif pushed_key == '3':
#                     Drink_balance=  Drink_balance +25#adds value in cents
#                     Drink_balance1= Drink_balance/100
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1#state transition
#                 elif pushed_key == '4':
#                     Drink_balance=  Drink_balance + 100#adds value in cents
#                     Drink_balance1= Drink_balance/100
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1#state transition
#                 elif pushed_key == '5':
#                     Drink_balance=  Drink_balance +500#adds value in cents
#                     Drink_balance1= Drink_balance/100
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1#state transition
#                 elif pushed_key == '6':
#                     Drink_balance=  Drink_balance +1000#adds value in cents
#                     Drink_balance1= Drink_balance/100
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1#state transition
#                 elif pushed_key == '7':
#                     Drink_balance=  Drink_balance +2000 #adds value in cents
#                     Drink_balance1= Drink_balance/100
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1#state transition
#                 elif pushed_key == 'E':
#                     print ("Eject Current Balance")
#                     state=5 #state transition
#                 elif pushed_key == 'C':
#                     print ("Cuke Selected")
#     ## Variable representing price of beverage in cents
#                     price= 100
#                     state=2 #state transition
#                 elif pushed_key == 'P':
#                     print ("Popsi selected")
#                     price= 125
#                     state=2 #state transition
#                 elif pushed_key == 'S':
#                     print ("Spryte selected")
#                     price= 150
#                     state=2#state transition
#                 elif pushed_key == 'D':
#                     print ("Dr Pupper selected")
#                     price= 175
#                     state=2#state transition
                    
#                 pushed_key = None

#         except KeyboardInterrupt:
#             break
        
#     elif state == 2:
#         """perform state 2 operations 
#         """
#         #which is checking if balance is sufficient for beverage.
#         if Drink_balance < price: #if current balance is less then price
#             state = 3# s2 -> s3
#         else: 
#             state=4 #state transition
        
        
#     elif state == 3:
#         """perform state 3 operations which is 
#         """
#   ## Variable representing price of beverage in dollars
#         price1= price/100
#         print(f"Insufficient Funds. Cost of item is $: {price1:.2f}")
#         print(f"Current balance is $: {Drink_balance1:.2f} Please add to balance or select new drink.")
#         # print('Insufficient Funds. Cost of item is $ '+str(price1) + ' . Current balance is $ '+str(Drink_balance1) +   '. Please add more money or select a different drink.')
#         state = 1# s3 -> s1
        
#         #update balance 
        
        
#     elif state == 4:
#         """perform state 3 operations
#         """
#         print('Drink Dispensed')
#         print('Please  wait for remaining balance to eject')
#         state = 5# s3 -> s1
#         # if pushed_key == 'C':
#         #     print ("Cuke Delivered. Balance Updated")
#         #     price= 1
#         #     payment= Drink_balance
#         #     getChange(price, payment)
#         #     state=2 
#         # elif pushed_key == 'P':
#         #     print ("Popsi")
#         #     price= 1.25
#         #     payment= Drink_balance
#         #     getChange(price, payment)
#         #     state=2 
#         # elif pushed_key == 'S':
#         #     print ("Spryte")
#         #     price= 1.50
#         #     payment= Drink_balance
#         #     getChange(price, payment)
#         #     state=2
#         # elif pushed_key == 'D':
#         #     print ("Dr Pupper")
#         #     price= 2
#         #     payment= Drink_balance
#         #     getChange(price, payment)
#         #     state=2
#         # else: 
#         #     state = 5
#         #     # How do I make it so that it actually waits for the users input
#         #     #and doesn't immediately run before they get a chance to do anything 
          
            
#     elif state == 5:
# ## Equating Function variable to the drink balance
       
#         payment= Drink_balance #setting function variable payment equal to current balance

# ## Variable representing change owed        
#         finalchange=getChange(price, payment) 
    
        
#         print ('Balance Ejected')
        
#         # Print a formatted string
#         print( ('Change:\n'
#                 ' %d pennies\n'
#                 ' %d nickels\n'
#                 ' %d dimes\n'
#                 ' %d quarters\n'
#                 ' %d $1 dollar bill\n'
#                 ' %d $5 dollar bill\n'
#                 ' %d $10 dollar bill\n'
#                 ' %d $20 dollar bill') % finalchange)
#         #print('Remaining balance is now'+str(getChange.change) + ' .')
        
#         Drink_balance=0
        
#         state=0
        
#     else:
#         pass
    
          


# #!/usr/bin/env python3
# # -*- coding: utf-8 -*-
# """
# @file Vendotron.py
# @brief       FSM for Vending machine Vendotron
# @details     This File takes in coins entered by the user and lets them select a beverage. It checkes if the payment is enough for the price of the beverage and if so 
#              it drops the beverage and ejects the remaining balance. 
# @author      Ashley Humpal
# @copyright   License Info
# @date        January 12, 2021
# """

# """Initialize code to run FSM (not the init state). This code initializes the program, not the FSM
# """
# ## The current balance in the Vendotron
# Drink_balance=0
# ## The first state of the FSM
# state = 0


# import keyboard

# ## Creation of variable for Key press
# pushed_key = None

# def on_keypress (thing):
#     """ Callback which runs when the user presses a key.  
#     """
#     global pushed_key

#     pushed_key = thing.name

# keyboard.on_press (on_keypress)  ########## Set callback



# def getChange(price, payment):
#     """ Computes change by taking the difference between a given price and payment given, both in numerical form. Returns change in a 8 length tuple ranging from the amount of pennies needed all the way to the number of $20 bills.
#     @brief           Computes change for monetary transaction
#     @details         Computes change given set of bills/coins and returns denominations
#     @param price     The price of the item as a numerical value in dollars
#     @param payment   The payment given for item as a numerical value in dollars
    
#     """

#     numericalchange= payment - price
#     change20 = numericalchange // 2000  #calculate how many 20's in change
#     newtot= numericalchange- 2000*(change20) #update total without the 20's
    
#     change10= newtot//1000 #calculate how many 20's in change
#     newtot1=newtot- 1000*(change10) #total updated
    
#     change5= newtot1//500 #calculate how many 5's in change
#     newtot2=newtot1- 500*(change5) #total updated
    
#     change1= newtot2//100 #calculate how many 1's in change
#     newtot3=newtot2- 100*(change1) #total updated
    
#     change25 = newtot3//25 #calculate how many quarters in change
#     newtot4=newtot3- 25*(change25) #total updated
    
#     changedime= newtot4//10 #calculate how many dimes in change
#     newtot5=newtot4- 10*(changedime) #total updated
    
#     changenickel = newtot5//5 #calculate how many nickels in change
#     newtot6=newtot5- 5*(changenickel) #total updated
    
#     changepenny = newtot6 // 1 #calculate how many pennies in change
#     newtot7=newtot6- 1*(changepenny) #total updated

#     change= (changepenny, changenickel, changedime, change25, change1, change5, change10, change20)
#     return change    

# def printWelcome():
#     """Prints a VendotronˆTMˆ welcome message with beverage selctions and prices. 
#     @brief          Welcomes user to Vendotron
#     @details        Gives instruction on how to enter certain bills and how to call for certain beverages
#     """
#     print('Welcome to VendotronˆTM. To begin with please insert change.')
#     print('''
#           Press 0 to add a penny,
#           1 for nickel, 
#           2 for dime,
#           3 for quarter,
#           4 for $1,
#           5 for $5,
#           6 for $10,
#           and 7 for $20.
#           ''')
#     print('After inserting money, please select a drink.')
#     print('''
#           Press C for cuke, $1;
#           P for Popsi, $1.25;
#           S for Spryte, $1.50;
#           D for Dr. Pupper, $1.75''' )
#     print(' To eject balance at any time please press E')
#     print('Thank You!!!')
    
          


# while True:
#     """Implement FSM using a while loop and an if statement
#     will run eternally until user presses CTRL-C
#     """
    
#     if state == 0: #if this state then do following
#         """perform state 0 operations
#         this init state, initializes the FSM itself, with all the
#         code features already set up
#         """
#         printWelcome()
#         state = 1
#     elif state == 1:#if this state then do following
#         """perform state 1 operations 
#         """
#         try:
#         #which is adding and displaying balance.
        
#         #try and accept block 
#             if pushed_key:
#                 if pushed_key == "0":
#                     Drink_balance=  Drink_balance + 1 #balance in cents
#                     Drink_balance1= Drink_balance/100 #balance in dollars
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1 #state transition
#                 elif pushed_key == '1':
#                     Drink_balance=  Drink_balance + 5 #balance in cents
#                     Drink_balance1= Drink_balance/100#balance in dollars
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1#state transition
#                 elif pushed_key == '2':
#                     Drink_balance=  Drink_balance + 10#balance in cents
#                     Drink_balance1= Drink_balance/100#balance in dollars
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1#state transition
#                 elif pushed_key == '3':
#                     Drink_balance=  Drink_balance +25#balance in cents
#                     Drink_balance1= Drink_balance/100#balance in dollars
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1#state transition
#                 elif pushed_key == '4':
#                     Drink_balance=  Drink_balance + 100#balance in cents
#                     Drink_balance1= Drink_balance/100#balance in dollars
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1#state transition
#                 elif pushed_key == '5':
#                     Drink_balance=  Drink_balance +500#balance in cents
#                     Drink_balance1= Drink_balance/100#balance in dollars
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1#state transition
#                 elif pushed_key == '6':
#                     Drink_balance=  Drink_balance +1000#balance in cents
#                     Drink_balance1= Drink_balance/100#balance in dollars
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1#state transition
#                 elif pushed_key == '7':
#                     Drink_balance=  Drink_balance +2000 #balance in cents
#                     Drink_balance1= Drink_balance/100#balance in dollars
#                     print(f"Current balance in dollars: {Drink_balance1:.2f}")
#                     state= 1#state transition
#                 elif pushed_key == 'E':
#                     print ("Eject Current Balance")
#                     state=5 #state transition
#                 elif pushed_key == 'C':
#                     print ("Cuke Selected")
#     ## Variable representing price of beverage
#                     price= 100
#                     state=2 #state transition
#                 elif pushed_key == 'P':
#                     print ("Popsi selected")
#                     price= 125
#                     state=2 #state transition
#                 elif pushed_key == 'S':
#                     print ("Spryte selected")
#                     price= 150
#                     state=2#state transition
#                 elif pushed_key == 'D':
#                     print ("Dr Pupper selected")
#                     price= 175
#                     state=2#state transition
                    
#                 pushed_key = None

#         except KeyboardInterrupt:
#             break
        
#     elif state == 2:#if this state then do following
#         """perform state 2 operations 
#         """
#         #which is checking if balance is sufficient for beverage.
#         if Drink_balance < price:
#             state = 3# s2 -> s3
#         else: 
#             state=4 #state transition
        
        
#     elif state == 3:#if this state then do following
#         """perform state 3 operations which is 
#         """
#         price1= price/100
#         print(f"Insufficient Funds. Cost of item is $: {price1:.2f}")
#         print(f"Current balance is $: {Drink_balance1:.2f} Please add to balance or select new drink.")
#        # print('Insufficient Funds. Cost of item is $ '+str(price1) + ' . Current balance is $ '+str(Drink_balance1) +   '. Please add more money or select a different drink.')
#         state = 1# s3 -> s1
#         #update balance 
        
        
#     elif state == 4:#if this state then do following
#         """perform state 3 operations
#         """
#         print('Drink Dispensed')
#         print('Please  wait for remaining balance to eject')
#         state = 5# s3 -> s1
#         # if pushed_key == 'C':
#         #     print ("Cuke Delivered. Balance Updated")
#         #     price= 1
#         #     payment= Drink_balance
#         #     getChange(price, payment)
#         #     state=2 
#         # elif pushed_key == 'P':
#         #     print ("Popsi")
#         #     price= 1.25
#         #     payment= Drink_balance
#         #     getChange(price, payment)
#         #     state=2 
#         # elif pushed_key == 'S':
#         #     print ("Spryte")
#         #     price= 1.50
#         #     payment= Drink_balance
#         #     getChange(price, payment)
#         #     state=2
#         # elif pushed_key == 'D':
#         #     print ("Dr Pupper")
#         #     price= 2
#         #     payment= Drink_balance
#         #     getChange(price, payment)
#         #     state=2
#         # else: 
#         #     state = 5
#         #     # How do I make it so that it actually waits for the users input
#         #     #and doesn't immediately run before they get a chance to do anything 
          
            
#     elif state == 5:#if this state then do following
# ## Equating Function variable to the drink balance
#         payment= Drink_balance #use function variables
#         finalchange=getChange(price, payment) #call function with parameters
#         print ('Balance Ejected')
#         # Print a formatted string
#         print( ('Change:\n'
#                 ' %d pennies\n'
#                 ' %d nickels\n'
#                 ' %d dimes\n'
#                 ' %d quarters\n'
#                 ' %d $1 dollar bill\n'
#                 ' %d $5 dollar bill\n'
#                 ' %d $10 dollar bill\n'
#                 ' %d $20 dollar bill') % finalchange)
#         #print('Remaining balance is now'+str(getChange.change) + ' .')
#         Drink_balance=0
#         state=0
        
#     else:
#         pass
    
          
        
        
        
    
        
        
    