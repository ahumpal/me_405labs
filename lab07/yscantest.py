#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 18:52:55 2021

@author: ashleyhumpal
"""
#yscan 


# import pyb
# from pyb import Pin
# from pyb import ADC
# PIN_xp = Pin (Pin.cpu.A7)
# PIN_ym= Pin(Pin.cpu.A0)
# PIN_yp = Pin (Pin.cpu.A1)
# ADC_xm = ADC(Pin.cpu.A6)
# PIN_xm= Pin (Pin.cpu.A6 , mode=Pin.IN)


# PIN_ym.init(mode=Pin.OUT_PP, value=0)
# PIN_yp.init(mode=Pin.OUT_PP, value=1)
# PIN_xp.init(mode=Pin.ANALOG)
# PIN_xm.init(mode=Pin.ANALOG)
# ADC_xm= ADC(PIN_xm)

# while True:
#     print(ADC_xm.read())
    


#x scan  
    
import pyb
from pyb import Pin
from pyb import ADC

PIN_yp = Pin (Pin.cpu.A1)
PIN_xm= Pin(Pin.cpu.A6)
PIN_xp = Pin (Pin.cpu.A7)
ADC_ym = ADC(Pin.cpu.A0)
PIN_ym= Pin (Pin.cpu.A0 , mode=Pin.IN)



PIN_xm.init(mode=Pin.OUT_PP, value=0)
PIN_xp.init(mode=Pin.OUT_PP, value=1)
PIN_yp.init(mode=Pin.ANALOG)
PIN_ym.init(mode=Pin.ANALOG)
ADC_ym= ADC(PIN_ym)
ADC_ym.read()


while True:
    print(ADC_ym.read())



            
            
# # # z test
# import pyb
# from pyb import Pin
# from pyb import ADC

# PIN_yp = Pin (Pin.cpu.A1)
# PIN_xm= Pin(Pin.cpu.A6)
# PIN_xp = Pin (Pin.cpu.A7)
# ADC_ym = ADC(Pin.cpu.A0)
# PIN_ym= Pin (Pin.cpu.A0 , mode=Pin.IN)



# PIN_xm.init(mode=Pin.OUT_PP, value=0)
# PIN_yp.init(mode=Pin.OUT_PP, value=1)

# PIN_xp.init(mode=Pin.ANALOG)
# PIN_ym.init(mode=Pin.ANALOG)

# ADC_ym= ADC(PIN_ym)
# ADC_ym.read()



# while True:
#     if ADC_ym.read() < 4000:
#         print(1)
#     else: 
#         print(0)

## Backup
# #!/usr/bin/env python3
# # -*- coding: utf-8 -*-

# """
# @file screendriver.py
# @brief       Code that serves as a driver to interface the resistive touch panel with the STM32 microcontroller.
# @details     Allows the user to set up the resistive touch panel with positive and negative pins
#              associated with the vertical and horizontal directions (y and x). Through 
#              reading changes in the voltage divider ratio, the contact point on the resistive panel 
#              can be read. This code includes methods to check if there has been any contact made
#              (returns 1 if yes, and 0 if no), the x position and the y position of a contact point. 
#              The user sets up several input parameter such as what pins they want for their positive and negative x's and y's,
#              the length and width of the plate, and the  x and y position of what they want to refer
#              to as their orgin. 
             
             
#              Link to Source Code: https://bitbucket.org/
                 
# @author      Ashley Humpal 
# @copyright   License Info
# @date        February 28, 2021
# """

# # import all necessary modules
# import pyb
# from pyb import Pin
# from pyb import ADC
# import utime

# ## New class representing the touch screen 
# class touchscreen:
#     '''
#     @brief      Class to represent the resistive touch panel
#     @details    This class implements a x, y, z, and combination scan which respectively 
#                 check the x coordinate of contact, y coordinate of contact, if any contact has
#                 been made, and finally a tuple of all of the previous three's results.
#                 . 
#     '''


      
#     def __init__(self, xp,xm,yp,ym, length, width, xorigin, yorigin):
#         # def __init__(self, xp,xm,yp,ym, length, width, center):
        
   
#         '''
#         @brief Creates methods to interface the resistive touch panel with the STM32 microcontroller
#         @param xp An object from class touchscreen representing the positive pin associated with the x direction
#         @param xm An object from class touchscreen representing the negative pin associated with the x direction
#         @param yp An object from class touchscreen representing the positive pin associated with the y direction
#         @param ym An object from class touchscreen representing the negative pin associated with the y direction
#         @param length An object from class touchscreen representing the length of the touchscreen
#         @param width An object from class touchscreen representing the width of the touchscreen
#         @param xorigin An object from class touchscreen representing the x position of the origin
#         @param yorigin An object from class touchscreen representing the y position of the origin
        
#         '''
# ## Variable representing the positive pin associated with the x direction
#         self.xp=xp  #initializing parameters
# ## Variable representing the negative pin associated with the x direction
#         self.xm=xm  #initializing parameters
# ## Variable representing the positive pin associated with the y direction
#         self.yp=yp #initializing parameters
# ## Variable representing the negative pin associated with the y direction
#         self.ym=ym  #initializing parameters
# # Variable representing the length of the touchscreen
#         self.length=length  #initializing parameters
# ## Variable representing the width of the touchscreen
#         self.width=width  #initializing parameters
# ## Variable representing the x position of the origin
#         self.xorigin=xorigin  #initializing parameters      
# ## Variable representing the y position of the origin
#         self.yorigin=yorigin  #initializing parameters            
# ## Intermediate variable initializing x scan value     
#         self.xbegin= 0 
# ## Intermediate variable initializing previous y scan value         
#         self.ybegin=0
        
        
#     def scanY(self): 
#         '''
#         @brief     Method that finds y coordinate of contact point
#         @details   Returns y coordinate in same units of input width
    
#         '''
# ## Variable representing Pin Xp
#         #self.PIN_xp = Pin (self.xp) # initializing pin
# ## Variable representing Pin Ym
#         #self.PIN_ym= Pin(self.ym) # initializing pin
# ## Variable representing Pin Yp
#         #self.PIN_yp = Pin (self.yp)# initializing pin
# ## Variable representing ADC Pin Xm
#         #self.ADC_xm = ADC(self.xm)# initializing pin that will be read from for ADC method
# ## Variable representing Pin Xm
#         #self.PIN_xm= Pin (self.xm , mode=Pin.IN) # setting pin as input 
        
# ## Variable representing Pin Ym as push-pull output pin and asserted low
#         Pin(self.ym).init(mode=Pin.OUT_PP, value=0) # make pin push-pull output and asserted low
# ## Variable representing Pin Yp as push-pull output pin and asserted high
#         Pin (self.yp).init(mode=Pin.OUT_PP, value=1) # make pin push-pull output and asserted high
# ## Variable representing Pin Xp as floating analog pin
#         Pin (self.xp).init(mode=Pin.ANALOG) #floating pin
# ## Variable representing Pin Xm as analog pin
#         #self.PIN_xm.init(mode=Pin.ANALOG) #pin to be measured from 
# ## Variable representing ADC Pin Xm 
#         self.ADC_xm= ADC(Pin (self.xm , mode=Pin.IN)) #set up pin for ADC method
        
#         pyb.udelay(4) #delay to account for settling time

# ## Variable representing current ADC value    
#         self.ynow= self.ADC_xm.read() #curent ADC value
# ## Variable representing average ADC reading
#         self.ynew= (1/2)*(self.ynow +self.ybegin) #Average current value with the previous beginning value
# ## Intermediate variable initializing previous y scan value 
#         self.ybegin= self.ADC_xm.read() # set beginning value as current 
        
#         #xcord=(self.ADC_ym.read()/4095)
#         #ycord=self.width*(self.ADC_xm.read()/4095)
#         #ycord=self.width*(self.ADC_xm.read()/4095)-self.yorigin 
        
        
# ## Variable representing Y coordinate of contact   
#         ycord=self.width*(self.ynew/4095)-self.yorigin #width linearly scaled with voltage value read minus origin to get absolute position
#         return ycord #return y  coordinate value
       
        
       

        
       
#     def scanX(self):
#         '''
#         @brief     Method that finds x coordinate of contact point
#         @details   Returns y coordinate in same units of input length
#         '''
# ## Variable representing Pin Yp
#         self.PIN_yp = Pin (self.yp) # initializing pin
# ## Variable representing Pin Xm    
#         self.PIN_xm= Pin(self.xm)# initializing pin
# ## Variable representing Pin Xp       
#         self.PIN_xp = Pin (self.xp)# initializing pin
# ## Variable representing ADC Pin Ym
#         self.ADC_ym = ADC(self.ym)# initializing pin
# ## Variable representing Pin Ym        
#         self.PIN_ym= Pin (self.ym , mode=Pin.IN)# setting pin as input 
# ## Variable representing Pin Xm as push-pull output pin and asserted low        
#         self.PIN_xm.init(mode=Pin.OUT_PP, value=0) # make pin push-pull output and asserted low
# ## Variable representing Pin Xp as push-pull output pin and asserted high
#         self.PIN_xp.init(mode=Pin.OUT_PP, value=1)# make pin push-pull output and asserted high
# ## Variable representing Pin Yp as floating analog pin        
#         self.PIN_yp.init(mode=Pin.ANALOG)#floating pin
# # ## Variable representing Pin Ym as analog pin        
# #         self.PIN_ym.init(mode=Pin.ANALOG)#pin to be measured from 
# ## Variable representing ADC Pin Ym         
#         self.ADC_ym= ADC(self.PIN_ym)#set up pin for ADC method
     
#         pyb.udelay(4)  #delay to account for settling time
# ## Variable representing current ADC value    
#         self.xnow= self.ADC_ym.read()
# ## Variable representing average ADC reading
#         self.xnew= (1/2)*(self.xnow +self.xbegin)
# ## Intermediate variable initializing previous X scan value 
#         self.xbegin= self.ADC_ym.read()
  
# ## Variable representing X coordinate of contact   
#         xcord=(self.length *(self.xnew/4095)) - self.xorigin #length linearly scaled with voltage value read minus origin to get absolute position
#         return xcord
        
#         # final_time = utime.ticks_us()
#         # full_time = utime.ticks_diff(final_time, begin_time)
#         # print(str(xcord) + ' {:}'. format(full_time))
#         # 176*normal read/4095 - x coord for origin , xz=l/2
#         # yc is w/2 
         

        
#     def scanZ(self):
#         '''
#         @brief     Returns value representing whether any contact was made with touchpanel
#         @details   Returns 1 if contact made, 0 if no contact made
#         '''
# ## Variable representing Pin Yp
#         self.PIN_yp = Pin (self.yp)
# ## Variable representing Pin Xm            
#         self.PIN_xm= Pin(self.xm)
# ## Variable representing Pin Xp          
#         self.PIN_xp = Pin (self.xp)
# ## Variable representing ADC Pin Ym        
#         self.ADC_ym = ADC(self.ym)
# ## Variable representing Pin Ym         
#         self.PIN_ym= Pin (self.ym , mode=Pin.IN)
        
        
# ## Variable representing Pin Xm as push-pull output pin and asserted low           
#         self.PIN_xm.init(mode=Pin.OUT_PP, value=0)
# ## Variable representing Pin Ym as push-pull output pin and asserted high
#         self.PIN_yp.init(mode=Pin.OUT_PP, value=1)
# ## Variable representing Pin Xp as floating analog pin 
#         self.PIN_xp.init(mode=Pin.ANALOG)
# ## Variable representing Pin Ym as analog pin  
#         self.PIN_ym.init(mode=Pin.ANALOG)
# ## Variable representing ADC Pin Ym         
#         self.ADC_ym= ADC(self.PIN_ym)
        
#         pyb.udelay(4)#delay to account for settling time
# ## Variable representing Y coordinate of contact  
#         ycord=(self.ADC_ym.read()) 
        
#         if self.ADC_ym.read() < 4000: #if read volt value less than 4000
#             zcord=1 #set z to 1 to indicate contact has been made since voltage has changed
#             return zcord
#         else: #otherwise
#             zcord=0  #z=0 because no contact has been made
#             return zcord
       
    
    
#     def scanCombo(self):
#         '''
#         @brief     Returns tuple of X, Y, and Z scan results 
#         @details   Formated as (X coordinate, Y coordinate, Z value)
#         '''
       
# ## Variable representing tuple of all coordinates       
#         totcord= (self.scanX(), self.scanY(), self.scanZ()) #calling methods needed 
#         return totcord
    
        
    
        
  

# if __name__=='__main__':
 
#     length=176
#     width= 100
#     #def __init__(self, xp,xm,yp,ym, length, width, xorigin, yorigin):
#     test1= touchscreen(Pin.cpu.A7,Pin.cpu.A6,Pin.cpu.A1,Pin.cpu.A0,length, width,length/2, width/2)
    
#     while True:
#         begin_time = utime.ticks_us()
#         test1.scanY()
#         final_time = utime.ticks_us()
#         full_time = utime.ticks_diff(final_time, begin_time)
#         print(str(test1.scanY()) + ' {:}'. format(full_time))
        
        
    
    
    
# #!/usr/bin/env python3
# # -*- coding: utf-8 -*-
# """
# @file touchpaneldriver.py
# @brief       Code that serves as a driver for the MCP9808 Temperature Sensor.
# @details     Allows the user to communicate with MCP9808 temperature sensor 
#              using its I2C interface. Includes methods to check if the sensor 
#              is attatched to the correct bus address, and register celsius and fahrenheit
#              temperature values. Takes I2C object and temperature sensor address
#              as input parameters.
             
#              Link to Source Code: https://bitbucket.org/rfunches/me-405-lab-4/src/master/mcp9808.py
                 
# @author      Ashley Humpal and Ryan Funchess
# @copyright   License Info
# @date        February 23, 2021
# """

# # import all necessary modules
# import pyb
# from pyb import Pin
# from pyb import ADC

# ## New class representing the touch screen 
# class touchscreen:
#     '''
#     @brief      Class to represent the resistive touch panel
#     @details    This class implements a check method to check if the sensor 
#                 is attatched to the correct bus address, and collects celsius and fahrenheit
#                 temperature values. 
#     '''


      
#     def __init__(self, xp,xm,yp,ym):
#         # def __init__(self, xp,xm,yp,ym, length, width, center):
        
   
#         '''
#         @brief Creates methods for interfacing with MCP9808 temperature sensor using its I2C interface.
#         @param I2object  An object from class tempsensor representing master device and bus.
#         @param Sensoraddress An object from class tempsensor representing the sensor device
        
#         '''
# ## Variable representing I2C master device
#         self.xp=xp  #initializing parameters
# ## Variable representing I2C master device
#         self.xm=xm  #initializing parameters
# ## Variable representing I2C master device
#         self.yp=yp #initializing parameters
# ## Variable representing I2C master device
#         self.ym=ym  #initializing parameters
# ## Variable representing I2C master device
# #         self.length=length  #initializing parameters
# # ## Variable representing I2C master device
# #         self.width=width  #initializing parameters
# # ## Variable representing I2C master device
# #         self.center=center  #initializing parameters
            
        
#         self.PIN_ym = Pin (Pin.cpu.ym)
#         self.PIN_yp = Pin (Pin.cpu.yp)
        
        
        
        
        
        
        
#     def scanX(self): 
#         '''
#         @brief     Verifies that the sensor is attached at the given bus address
#         @details   Checks that the value in the manufacturer ID register is correct
    
#         '''
# ## Variable representing state of connection from master to sensor
#         self.PIN_xm= (Pin.cpu.xm)
#         self.PIN_xp = Pin (Pin.cpu.xp)
#         self.PIN_ym= Pin (Pin.cpu.ym , mode=Pin.IN) #this is whats being checked for value
        
#         self.PIN_xm.init(mode=Pin.OUT_PP, value=0)  # x is output 
#         self.PIN_xp.init(mode=Pin.OUT_PP, value=1) # x is output 
#         self.PIN_ym.init(mode=Pin.ANALOG)
        
#         self.PIN_yp.init(mode=Pin.ANALOG)  # free pin here is yp-what do I do here??
        
#         self.ADC_ym= ADC(self.PIN_ym)
#         self.ADC_ym.read()
#         while True:
#             print(self.ADC_ym.read()/4095)
       
        
        
    
#     def scanY(self):
#         '''
#         @brief     Returns measured temperature in degrees Celsius
#         @details   Converts raw temperature data from bytes to numerical value
#         '''
# ## Var
#         self.PIN_ym= (Pin.cpu.ym)
#         self.PIN_yp = Pin (Pin.cpu.yp)
#         self.PIN_xm= Pin (Pin.cpu.A1 , mode=Pin.IN) #this is whats being checked for value
        
#         self.PIN_ym.init(mode=Pin.OUT_PP, value=0)  # x is output 
#         self.PIN_yp.init(mode=Pin.OUT_PP, value=1) # x is output 
#         self.PIN_xm.init(mode=Pin.ANALOG)
        
#         self.PIN_xp.init(mode=Pin.ANALOG)  # free pin here is xp-what do I do here??
        
#         self.ADC_xm= ADC(self.PIN_xm)
#         self.ADC_xm.read()
#         while True:
#             print(self.ADC_xm.read()/4095)
        
        
#     def scanZ(self):
#         '''
#         @brief     Returns measured temperature in degrees Celsius
#         @details   Converts raw temperature data from bytes to numerical value
#         '''
# ## Var
#         self.PIN_yp= (Pin.cpu.yp)
#         self.PIN_yp.init(mode=Pin.OUT_PP, value=1)  # is value = 1 making it high?
        
#         self.PIN_xm= (Pin.cpu.xm)
#         self.PIN_xm.init(mode=Pin.OUT_PP, value=0)  # x is output 
        
#         self.PIN_xp.init(mode=Pin.ANALOG)
        
#         self.PIN_ym= (Pin.cpu.ym)
        
#         self.ADC_ym= ADC(self.PIN_ym)
#         self.ADC_ym.read()
        
#         while True:
#             print(self.ADC_ym.read()/4095)

# if __name__=='__main__':
#     from touchpaneldriver import touchscreen
#     A6=pyb.Pin.cpu
   
#     test1= touchscreen(A6,A7,A0,A1)
#     test1.scanY()

    