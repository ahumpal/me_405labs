#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file touchpaneldriver.py
@brief       Code that serves as a driver for the MCP9808 Temperature Sensor.
@details     Allows the user to communicate with MCP9808 temperature sensor 
             using its I2C interface. Includes methods to check if the sensor 
             is attatched to the correct bus address, and register celsius and fahrenheit
             temperature values. Takes I2C object and temperature sensor address
             as input parameters.
             
             Link to Source Code: https://bitbucket.org/rfunches/me-405-lab-4/src/master/mcp9808.py
                 
@author      Ashley Humpal and Ryan Funchess
@copyright   License Info
@date        February 23, 2021
"""

# import all necessary modules
import pyb
from pyb import Pin
from pyb import ADC

## New class representing the touch screen 
class touchscreen:
    '''
    @brief      Class to represent the resistive touch panel
    @details    This class implements a check method to check if the sensor 
                is attatched to the correct bus address, and collects celsius and fahrenheit
                temperature values. 
    '''


      
    def __init__(self, xp,xm,yp,ym):
        # def __init__(self, xp,xm,yp,ym, length, width, center):
        
   
        '''
        @brief Creates methods for interfacing with MCP9808 temperature sensor using its I2C interface.
        @param I2object  An object from class tempsensor representing master device and bus.
        @param Sensoraddress An object from class tempsensor representing the sensor device
        
        '''
## Variable representing I2C master device
        self.xp=xp  #initializing parameters
## Variable representing I2C master device
        self.xm=xm  #initializing parameters
## Variable representing I2C master device
        self.yp=yp #initializing parameters
## Variable representing I2C master device
        self.ym=ym  #initializing parameters
## Variable representing I2C master device
#         self.length=length  #initializing parameters
# ## Variable representing I2C master device
#         self.width=width  #initializing parameters
# ## Variable representing I2C master device
#         self.center=center  #initializing parameters
            
        
        
    def scanY(self): 
        '''
        @brief     Verifies that the sensor is attached at the given bus address
        @details   Checks that the value in the manufacturer ID register is correct
    
        '''
## Variable representing state of connection from master to sensor
        self.PIN_xp = Pin (self.xp)
        self.PIN_ym= Pin(self.ym)
        self.PIN_yp = Pin (self.yp)
        self.ADC_xm = ADC(Pin.cpu.A6)
        self.PIN_xm= Pin (self.xm , mode=Pin.IN)
        
        
        self.PIN_ym.init(mode=Pin.OUT_PP, value=0)
        self.PIN_yp.init(mode=Pin.OUT_PP, value=1)
        self.PIN_xp.init(mode=Pin.ANALOG)
        self.PIN_xm.init(mode=Pin.ANALOG)
        self.ADC_xm= ADC(self.PIN_xm)
        
        while True:
            print(self.ADC_xm.read())
       
        
       
    def scanX(self):
        '''
        @brief     Returns measured temperature in degrees Celsius
        @details   Converts raw temperature data from bytes to numerical value
        '''
## Var
        self.PIN_yp = Pin (self.ym)
        self.PIN_xm= Pin(self.xm)
        self.PIN_xp = Pin (self.xp)
        self.ADC_ym = ADC(self.ym)
        self.PIN_ym= Pin (self.ym , mode=Pin.IN)
        
        
        
        self.PIN_xm.init(mode=Pin.OUT_PP, value=0)
        self.PIN_xp.init(mode=Pin.OUT_PP, value=1)
        self.PIN_yp.init(mode=Pin.ANALOG)
        self.PIN_ym.init(mode=Pin.ANALOG)
        self.ADC_ym= ADC(self.PIN_ym)
        self.ADC_ym.read()
        
        
        while True:
            print(self.ADC_ym.read())
                
        
    def scanZ(self):
        '''
        @brief     Returns measured temperature in degrees Celsius
        @details   Converts raw temperature data from bytes to numerical value
        '''
        self.PIN_yp = Pin (self.yp)
        self.PIN_xm= Pin(self.xm)
        self.PIN_xp = Pin (self.xp)
        self.ADC_ym = ADC(self.ym)
        self.PIN_ym= Pin (self.ym , mode=Pin.IN)
        
        
        
        self.PIN_xm.init(mode=Pin.OUT_PP, value=0)
        self.PIN_yp.init(mode=Pin.OUT_PP, value=1)
        
        self.PIN_xp.init(mode=Pin.ANALOG)
        self.PIN_ym.init(mode=Pin.ANALOG)
        
        self.ADC_ym= ADC(self.PIN_ym)
        self.ADC_ym.read()
        
        while True:
            if self.ADC_ym.read() < 4000:
                print(1)
            else: 
                print(0)


    def scanCombo(self):
        '''
        @brief     Returns measured temperature in degrees Celsius
        @details   Converts raw temperature data from bytes to numerical value
        '''
        self.PIN_yp = Pin (self.yp)
        self.PIN_xm= Pin(self.xm)
        self.PIN_xp = Pin (self.xp)
        self.ADC_ym = ADC(self.ym)
        self.PIN_ym= Pin (self.ym , mode=Pin.IN)
        
        
        
        self.PIN_xm.init(mode=Pin.OUT_PP, value=0)
        self.PIN_yp.init(mode=Pin.OUT_PP, value=1)
        
        self.PIN_xp.init(mode=Pin.ANALOG)
        self.PIN_ym.init(mode=Pin.ANALOG)
        
        self.ADC_ym= ADC(self.PIN_ym)
        self.ADC_ym.read()
        
        while True:
            if self.ADC_ym.read() < 4000:
                print(1)
            else: 
                print(0)
if __name__=='__main__':
    from touchpaneldriver import touchscreen
    A6=pyb.Pin.cpu
   
    test1= touchscreen(Pin.cpu.A6,Pin.cpu.A7,Pin.cpu.A0,Pin.cpu.A1)
    test1.scanY()
    
    
# #!/usr/bin/env python3
# # -*- coding: utf-8 -*-
# """
# @file touchpaneldriver.py
# @brief       Code that serves as a driver for the MCP9808 Temperature Sensor.
# @details     Allows the user to communicate with MCP9808 temperature sensor 
#              using its I2C interface. Includes methods to check if the sensor 
#              is attatched to the correct bus address, and register celsius and fahrenheit
#              temperature values. Takes I2C object and temperature sensor address
#              as input parameters.
             
#              Link to Source Code: https://bitbucket.org/rfunches/me-405-lab-4/src/master/mcp9808.py
                 
# @author      Ashley Humpal and Ryan Funchess
# @copyright   License Info
# @date        February 23, 2021
# """

# # import all necessary modules
# import pyb
# from pyb import Pin
# from pyb import ADC

# ## New class representing the touch screen 
# class touchscreen:
#     '''
#     @brief      Class to represent the resistive touch panel
#     @details    This class implements a check method to check if the sensor 
#                 is attatched to the correct bus address, and collects celsius and fahrenheit
#                 temperature values. 
#     '''


      
#     def __init__(self, xp,xm,yp,ym):
#         # def __init__(self, xp,xm,yp,ym, length, width, center):
        
   
#         '''
#         @brief Creates methods for interfacing with MCP9808 temperature sensor using its I2C interface.
#         @param I2object  An object from class tempsensor representing master device and bus.
#         @param Sensoraddress An object from class tempsensor representing the sensor device
        
#         '''
# ## Variable representing I2C master device
#         self.xp=xp  #initializing parameters
# ## Variable representing I2C master device
#         self.xm=xm  #initializing parameters
# ## Variable representing I2C master device
#         self.yp=yp #initializing parameters
# ## Variable representing I2C master device
#         self.ym=ym  #initializing parameters
# ## Variable representing I2C master device
# #         self.length=length  #initializing parameters
# # ## Variable representing I2C master device
# #         self.width=width  #initializing parameters
# # ## Variable representing I2C master device
# #         self.center=center  #initializing parameters
            
        
#         self.PIN_ym = Pin (Pin.cpu.ym)
#         self.PIN_yp = Pin (Pin.cpu.yp)
        
        
        
        
        
        
        
#     def scanX(self): 
#         '''
#         @brief     Verifies that the sensor is attached at the given bus address
#         @details   Checks that the value in the manufacturer ID register is correct
    
#         '''
# ## Variable representing state of connection from master to sensor
#         self.PIN_xm= (Pin.cpu.xm)
#         self.PIN_xp = Pin (Pin.cpu.xp)
#         self.PIN_ym= Pin (Pin.cpu.ym , mode=Pin.IN) #this is whats being checked for value
        
#         self.PIN_xm.init(mode=Pin.OUT_PP, value=0)  # x is output 
#         self.PIN_xp.init(mode=Pin.OUT_PP, value=1) # x is output 
#         self.PIN_ym.init(mode=Pin.ANALOG)
        
#         self.PIN_yp.init(mode=Pin.ANALOG)  # free pin here is yp-what do I do here??
        
#         self.ADC_ym= ADC(self.PIN_ym)
#         self.ADC_ym.read()
#         while True:
#             print(self.ADC_ym.read()/4095)
       
        
        
    
#     def scanY(self):
#         '''
#         @brief     Returns measured temperature in degrees Celsius
#         @details   Converts raw temperature data from bytes to numerical value
#         '''
# ## Var
#         self.PIN_ym= (Pin.cpu.ym)
#         self.PIN_yp = Pin (Pin.cpu.yp)
#         self.PIN_xm= Pin (Pin.cpu.A1 , mode=Pin.IN) #this is whats being checked for value
        
#         self.PIN_ym.init(mode=Pin.OUT_PP, value=0)  # x is output 
#         self.PIN_yp.init(mode=Pin.OUT_PP, value=1) # x is output 
#         self.PIN_xm.init(mode=Pin.ANALOG)
        
#         self.PIN_xp.init(mode=Pin.ANALOG)  # free pin here is xp-what do I do here??
        
#         self.ADC_xm= ADC(self.PIN_xm)
#         self.ADC_xm.read()
#         while True:
#             print(self.ADC_xm.read()/4095)
        
        
#     def scanZ(self):
#         '''
#         @brief     Returns measured temperature in degrees Celsius
#         @details   Converts raw temperature data from bytes to numerical value
#         '''
# ## Var
#         self.PIN_yp= (Pin.cpu.yp)
#         self.PIN_yp.init(mode=Pin.OUT_PP, value=1)  # is value = 1 making it high?
        
#         self.PIN_xm= (Pin.cpu.xm)
#         self.PIN_xm.init(mode=Pin.OUT_PP, value=0)  # x is output 
        
#         self.PIN_xp.init(mode=Pin.ANALOG)
        
#         self.PIN_ym= (Pin.cpu.ym)
        
#         self.ADC_ym= ADC(self.PIN_ym)
#         self.ADC_ym.read()
        
#         while True:
#             print(self.ADC_ym.read()/4095)

# if __name__=='__main__':
#     from touchpaneldriver import touchscreen
#     A6=pyb.Pin.cpu
   
#     test1= touchscreen(A6,A7,A0,A1)
#     test1.scanY()