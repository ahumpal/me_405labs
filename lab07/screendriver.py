#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@file screendriver.py
@brief       Code that serves as a driver to interface the resistive touch panel with the STM32 microcontroller.
@details     Allows the user to set up the resistive touch panel with positive and negative pins
             associated with the vertical and horizontal directions (y and x). Through 
             reading changes in the voltage divider ratio, the contact point on the resistive panel 
             can be read. This code includes methods to check if there has been any contact made
             (returns 1 if yes, and 0 if no), the x position and the y position of a contact point. 
             The user sets up several input parameters such as what pins they want for their positive and negative x's and y's,
             the length and width of the plate, and the  x and y position of what they want to refer
             to as their orgin. Note that conversions have been added that are specific to my board to normalize my values,
             so those need to be removed for other boards (place noted in code).
             
             Link to Source Code: https://bitbucket.org/ahumpal/me_405labs/src/master/lab07/screendriver.py
                 
@author      Ashley Humpal 
@copyright   License Info
@date        February 28, 2021
"""

# import all necessary modules
import pyb
from pyb import Pin
from pyb import ADC
import utime

## New class representing the touch screen 
class touchscreen:
    '''
    @brief      Class to represent the resistive touch panel.
    @details    This class implements a x, y, z, and combination scan which respectively 
                check the x coordinate of contact, y coordinate of contact, if any contact has
                been made, and finally a tuple of all of the previous three's results.
                . 
    '''


      
    def __init__(self, xp,xm,yp,ym, length, width, xorigin, yorigin):
        # def __init__(self, xp,xm,yp,ym, length, width, center):
        
   
        '''
        @brief Creates methods to interface the resistive touch panel with the STM32 microcontroller
        @param xp An object from class touchscreen representing the positive pin associated with the x direction
        @param xm An object from class touchscreen representing the negative pin associated with the x direction
        @param yp An object from class touchscreen representing the positive pin associated with the y direction
        @param ym An object from class touchscreen representing the negative pin associated with the y direction
        @param length An object from class touchscreen representing the length of the touchscreen
        @param width An object from class touchscreen representing the width of the touchscreen
        @param xorigin An object from class touchscreen representing the x position of the origin
        @param yorigin An object from class touchscreen representing the y position of the origin
        
        '''
## Variable representing the positive pin associated with the x direction
        self.xp=xp  #initializing parameters
## Variable representing the negative pin associated with the x direction
        self.xm=xm  #initializing parameters
## Variable representing the positive pin associated with the y direction
        self.yp=yp #initializing parameters
## Variable representing the negative pin associated with the y direction
        self.ym=ym  #initializing parameters
## Variable representing the length of the touchscreen
        self.length=length  #initializing parameters
## Variable representing the width of the touchscreen
        self.width=width  #initializing parameters
## Variable representing the x position of the origin
        self.xorigin=xorigin  #initializing parameters      
## Variable representing the y position of the origin
        self.yorigin=yorigin  #initializing parameters            
## Intermediate variable initializing x scan value     
        self.xbegin= 0 
## Intermediate variable initializing previous y scan value         
        self.ybegin=0
        
        
    def scanY(self): 
        '''
        @brief     Method that finds y coordinate of contact point
        @details   Returns y coordinate in same units of input width
    
        '''     
## Variable representing Pin Ym as push-pull output pin and asserted low
        Pin(self.ym).init(mode=Pin.OUT_PP, value=0) # make pin push-pull output and asserted low
## Variable representing Pin Yp as push-pull output pin and asserted high
        Pin (self.yp).init(mode=Pin.OUT_PP, value=1) # make pin push-pull output and asserted high
## Variable representing Pin Xp as floating analog pin
        Pin (self.xp).init(mode=Pin.ANALOG) #floating pin

## Variable representing ADC Pin Xm 
        self.ADC_xm= ADC(Pin (self.xm , mode=Pin.IN)) #set up pin for ADC method
        
        pyb.udelay(4) #delay to account for settling time


# Moving Average Filter 
## Variable representing current ADC value    
        self.ynow= self.ADC_xm.read() #curent ADC value
## Variable representing average ADC reading
        self.ynew= (1/2)*(self.ynow +self.ybegin) #Average current value with the previous beginning value to filter
## Intermediate variable initializing previous y scan value 
        self.ybegin= self.ADC_xm.read() # set beginning value as current 
        
        
## Variable representing Y coordinate of contact   
        ycord=(self.width*(self.ynew/4095)-self.yorigin) #width linearly scaled with voltage value read minus origin to get absolute position
        #the 52.5/38 conversion is specific to my board because the count maxes were off so I had to normalize values and it peaked at 38 instead of 52.5
        
        # If using board that's not mine simply take out the (52.5/38) conversion
        
        return ycord #return y  coordinate value
       



    def scanX(self):
        '''
        @brief     Method that finds x coordinate of contact point
        @details   Returns x coordinate in same units of input length
        '''
## Variable representing Pin Xm as push-pull output pin and asserted low        
        Pin(self.xm).init(mode=Pin.OUT_PP, value=0) # make pin push-pull output and asserted low
## Variable representing Pin Xp as push-pull output pin and asserted high
        Pin (self.xp).init(mode=Pin.OUT_PP, value=1)# make pin push-pull output and asserted high
## Variable representing Pin Yp as floating analog pin        
        Pin (self.yp).init(mode=Pin.ANALOG)#floating pin
# ## Variable representing Pin Ym as analog pin        
#         self.PIN_ym.init(mode=Pin.ANALOG)#pin to be measured from 
## Variable representing ADC Pin Ym         
        self.ADC_ym= ADC(Pin (self.ym , mode=Pin.IN))#set up pin for ADC method
     
        pyb.udelay(4)  #delay to account for settling time

# Moving Average Filter         
## Variable representing current ADC value    
        self.xnow= self.ADC_ym.read() #curent ADC value
## Variable representing average ADC reading
        self.xnew= (1/2)*(self.xnow +self.xbegin)#Average current value with the previous beginning value to filter
## Intermediate variable initializing previous X scan value 
        self.xbegin= self.ADC_ym.read()
  
## Variable representing X coordinate of contact   
        xcord=((self.length *(self.xnew/4095)) - self.xorigin) #length linearly scaled with voltage value read minus origin to get absolute position
        #the 88/77 conversion is specific to my board because the count maxes were off so I had to normalize values
        
        # If using board that's not mine simply take out the (88/77) conversion
        
        return xcord



        
    def scanZ(self):
        '''
        @brief     Returns value representing whether any contact was made with touchpanel
        @details   Returns 1 if contact made, 0 if no contact made
        '''       
## Variable representing Pin Xm as push-pull output pin and asserted low           
        Pin(self.xm).init(mode=Pin.OUT_PP, value=0)
## Variable representing Pin Ym as push-pull output pin and asserted high
        Pin (self.yp).init(mode=Pin.OUT_PP, value=1)
## Variable representing Pin Xp as floating analog pin 
        Pin (self.xp).init(mode=Pin.ANALOG)

## Variable representing ADC Pin Ym         
        self.ADC_ym= ADC(Pin (self.ym , mode=Pin.IN))
        
        pyb.udelay(4)#delay to account for settling time
        
## Variable representing Y coordinate of contact  
        ycord=(self.ADC_ym.read()) 
        
        if self.ADC_ym.read() < 4000: #if read volt value less than 4000
## Variable representing z coordinate
            zcord=1 #set z to 1 to indicate contact has been made since voltage has changed
            return zcord
        else: #otherwise
            zcord=0  #z=0 because no contact has been made
            return zcord
       
    
    
    
    
    def scanCombo(self):
        '''
        @brief     Returns tuple of X, Y, and Z scan results 
        @details   Formated as (X coordinate, Y coordinate, Z value)
        '''
       
## Variable representing tuple of all coordinates       
        totcord= (self.scanX(), self.scanY(), self.scanZ()) #calling methods needed 
        return totcord
    
    

# if __name__=='__main__':
 
#     length=176
#     width= 100
    
#     test1= touchscreen(Pin.cpu.A7,Pin.cpu.A6,Pin.cpu.A1,Pin.cpu.A0,length, width,length/2, width/2)

#     while True:
#         begin_time = utime.ticks_us()
#         test1.scanCombo()
#         final_time = utime.ticks_us()
#         full_time = utime.ticks_diff(final_time, begin_time)
#         print( '(X Y Z), Time (micro sec), Time (micro sec):' +str(test1.scanCombo()) + ' {:}'. format(full_time))
        
        
